import {
  ADMIN,
  FETCH_PHOTO_LIST,
  FETCH_PHOTO_ALL,
  PUT_REMOVE_PHOTO,
  PUT_UPDATE_PHOTO
} from '../constants/action-constants';

export const fetchPhotoList = (pageNo)=>({
  type : FETCH_PHOTO_LIST,
  route : `photos/list-top/${pageNo}`,
  domain : ADMIN,
})

export const putRemovePhoto = (data)=>({
  type : PUT_REMOVE_PHOTO,
  route : `photos/remove`,
  domain : ADMIN,
  data,
})

export const putUpdatePhoto = (data)=>({
  type : PUT_UPDATE_PHOTO,
  route : `photos/update`,
  domain : ADMIN,
  data,
})

export const fetchAllPhotos = ()=>({
  type : FETCH_PHOTO_ALL,
  route : `photos/list-all`,
  domain : ADMIN,
})
