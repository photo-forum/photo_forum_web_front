//abhishek360

import {
  FETCH_USER_EVENTS,
  FETCH_USER_EVENTS_SUCCESS,
  FETCH_USER_EVENTS_FAILURE,
  FETCH_USER_EVENTS_ERROR,
} from '../constants/action-constants';

const STATE = {
  count: 0,
  rows: [],
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case FETCH_USER_EVENTS:
      console.log('get user reg events dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case FETCH_USER_EVENTS_SUCCESS:
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case FETCH_USER_EVENTS_FAILURE:
      console.log('get user reg events failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case FETCH_USER_EVENTS_ERROR:
      console.log('get user reg events error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
