//abhishek360
import React from "react";
//import { connect }  from 'react-redux';
import RequestService from '../services/RequestService';

export function requireAuth(Component) {
  return class AuthenticatedComponent extends React.Component {
    state = {
      loggedIn: false,
    }

    constructor(){
      super();
      this.requestService = new RequestService('users','ADMIN')
    }

    componentDidMount() {
      this.requestService.loggedIn().then((res)=> {
        if(res){
          this.setState({
            loggedIn: true,
          })
        }
      })
    }

    // isAuthenticated(firstName) {
    //   if(firstName&&firstName.length>0)
    //     return true;
    //   else
    //     return false;
    // }

    render() {
      const loggedIn = this.state.loggedIn;
      const loginErrorMessage = (
        <div align = 'center' style = {{ fontSize: 25, marginTop: '15%'}}>
          <h2>Unauthorized!</h2><br/>Please login to view this content.
        </div>
      );

      return (
        <div>
          { loggedIn ? <Component {...this.props} /> : loginErrorMessage }
        </div>
      );
    }
  };


  // const mapStateToProps = ({ userDetails }) => ({ userDetails });
  //
  // return connect(mapStateToProps, {
  //
  // })(AuthenticatedComponent);
}
