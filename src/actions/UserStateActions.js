import {
  ADMIN,
  FETCH_USER_DETAILS,
  PUT_DETAILS_UPDATE,
  POST_CHANGE_PSSWD,
  FETCH_UPLOAD_URL,
  RESET_UPDATE_STATE,
  FETCH_USER_EVENTS,
  PUT_USERNAME_UPDATE,
  USER_LOGOUT,
} from '../constants/action-constants';

export const fetchUserDetails = ()=>({
  type : FETCH_USER_DETAILS,
  route : `users/details`,
  domain : ADMIN,
})

export const fetchUserEvents = (pageNo)=>({
  type : FETCH_USER_EVENTS,
  route : `users/registered-events/${pageNo}`,
  domain : ADMIN,
})

export const fetchUploadUrl = (query)=>({
  type : FETCH_UPLOAD_URL,
  route : `users/upload-url`,
  domain : ADMIN,
  query: '?key='+query
})

export const resetUpdateReducer = ()=>({
  type : RESET_UPDATE_STATE,
})

export const putDetailsUpdate = (data)=>({
  type : PUT_DETAILS_UPDATE,
  route : `users/update`,
  domain : ADMIN,
  data
})

export const putUpdateUsername = (data)=>({
  type : PUT_USERNAME_UPDATE,
  route : `users/username-update`,
  domain : ADMIN,
  data
})

export const postChangePsswd = (oldPassword, newPassword)=>({
  type : POST_CHANGE_PSSWD,
  route : `users/change_password`,
  domain : ADMIN,
  data : {
    oldPassword,
    newPassword
  }
})

export const userLogout = ( ) => ({
  type: USER_LOGOUT,
});
