//abhishek360

const STATE = {
  photowalks: [],
  competitions: [],
  workshops: [],
  exhibitions: [],
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case 'PUT_LIST_TOP':
      console.log('put list top dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case 'PUT_LIST_TOP_SUCCESS':
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case 'PUT_LIST_TOP_FAILURE':
      console.log('put list top failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case 'PUT_LIST_TOP_ERROR':
      console.log('put list top error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
