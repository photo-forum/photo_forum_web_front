//abhishek360

const STATE = {
  list: [],
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case 'PUT_LIST_ALL':
      console.log('put list all dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case 'PUT_LIST_ALL_SUCCESS':
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case 'PUT_LIST_ALL_FAILURE':
      console.log('put list all failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case 'PUT_LIST_ALL_ERROR':
      console.log('put list all error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
