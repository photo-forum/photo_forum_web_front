//abhishek360

import React from 'react';
import {
  Box,
  Grid,
  Typography,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  CardActions,
} from '@material-ui/core'
import {
  MoodBad,
  Mood,
} from '@material-ui/icons'
import * as Colors from '../../configs/colors';
import {
  renderDateTime,
  generateEventState
} from '../../helpers/UsefullFunctions';
import EventStateHOC from '../../HOC/EventStateHOC';

class EventCategory extends React.Component {
  renderNoEvents = () => {
    return (
      <Grid
        item lg = {3}
      >
        <Card style = {{ backgroundColor: Colors.AQUAMARINE, margin: 25,}}>
          <CardContent
            style = {{
              minWidth: 235,
              minHeight: 235,
              margin: 5,
              padding: 10,
              border: '2px solid black'
            }}
          >
            <Typography
              gutterBottom
              variant = 'h5'
              align = 'center'
              style = {{ margin: 10, padding: 5}}
            >
              <MoodBad style = {{height: 70, width: 70}}/>
              <br/>
              <br/>
              It's empty here, no events added yet.
            </Typography>
          </CardContent>
        </Card>
      </Grid>
    )
  }

  renderViewMore = () => {
    return (
      <Grid
        item lg = {3}
      >
        <Card style = {{backgroundColor: Colors.AQUAMARINE, margin: 25,}}>
          <CardActionArea style = { {} }>
            <CardContent
              style = {{
                minWidth: 235,
                minHeight: 235,
                margin: 5,
                padding: 10,
                border: '2px solid black'
              }}
            >
              <Typography
                gutterBottom
                align = 'center'
                style = {{ margin: 10, padding: 5}}
                variant = 'h5'
              >
                <Mood style = {{height: 70, width: 70}}/>
                <br/>
                <br/>
                There is lot more to explore, click to view more events.
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
    )
  }

  render() {
    const title = this.props.categoryTitle;
    return (
      <div style = { styles.container }>
        <Box
          style = { styles.titleText }
        >
          <div style = {{display: 'inline', fontSize: 35}}>{title.charAt(0)}</div>
          <div style = {{display: 'inline', fontSize: 20}}>{title.slice(1, title.length)}</div>
        </Box>
        <Grid
          container
          direction = 'row'
          justify = 'space-evenly'
          alignItems = 'center'
          style = {{ height: '90%'}}
        >
          {
            this.props.categoryEvents.map(item => {
              const eventState = generateEventState(item.registerAt, item.startsAt, item.endsAt);
              return (
                <Grid
                  item lg = {3}
                  key = { item.id }
                >
                <Card style = {{ margin: 25,}}>
                  <CardActionArea style = { styles.actionAreaCard }>
                    <CardMedia
                      component = "img"
                      alt = "asdfghj"
                      style = {{maxHeight: 180, maxWidth: 240, height: '100%', width: '100%' }}
                      image = "https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/slide3.jpg"
                      tittle = 'Photography'
                    />
                    <CardContent style = {{padding: 5}}>
                      <Typography
                        variant = 'h6'
                      >
                        { item.title }
                      </Typography>
                      <Typography
                        variant = 'caption'
                      >
                        Starts At: <b>{renderDateTime(item.startsAt)}</b>
                      </Typography>
                      <br/>
                      <Typography
                        variant = 'caption'
                      >
                        { item.desc }
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  <CardActions style = {styles.cardAction}>
                    <EventStateHOC
                      eventState = {eventState}
                      registerAt = {item.registerAt}
                    />
                  </CardActions>
                </Card>
              </Grid>
              )
            })
          }

          {
            (this.props.categoryEvents.length === 0)&& this.renderNoEvents()
          }
          {
            (this.props.categoryEvents.length >= 3)&&this.renderViewMore()
          }
        </Grid>
      </div>
    );
  }
}

const styles = {
  container: {
    marginLeft: '15%',
    marginBottom: 20,
    width: '70%'
  },
  titleText: {
    fontSize: 16,
    padding: 5,
    color: 'white',
    borderRadius: 2,
    background: 'linear-gradient(45deg, #EA638C 30%, #FFFFFF 90%)',
  },
  actionAreaCard: {
    minWidth: 240,
    minHeight: 200,
    maxHeight:  240,
  },
  cardAction: {
    display: 'flex',
    flexDirection: 'horizontal',
    justifyContent: 'space-between',
    backgroundColor: Colors.AQUAMARINE,
  },
  button: {
    padding: 5,
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    color: Colors.WHITE,
    background: Colors.FOREGROUND,
  },
};


export default EventCategory;
