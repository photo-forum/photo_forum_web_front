//abhishek360

import React, { Component } from 'react';
import * as Colors from '../../configs/colors';
import {
  Typography,
  Card,
  CardContent,
  CardActionArea,
  CardMedia,
} from '@material-ui/core';
import { RatingSlider } from './RatingSlider';
import { connect } from 'react-redux';
import {

} from '@material-ui/icons';
import {
  renderDateTime
} from '../../helpers/UsefullFunctions';

class CreativitySection extends Component{
  state = {
    value: 0,
  }

  handleChange = (event, value) => {
    this.setState({
      value: value
    })
  }

  renderPhotosList = (item, key) => {
    return (
      <Card key = {key} style = {styles.card}>
        <CardActionArea
          align = 'center'
          style = { styles.actionAreaCard }
          onClick = {()=> this.props.toggleCreativityDetails(item)}
        >
          <div style = {styles.caption}>
            <Typography
              style = {{fontSize: 25}}
            >
              { item.caption }
            </Typography>
            <Typography
              style = {{fontSize: 12}}
            >
              {renderDateTime(item.createdAt)}
            </Typography>
            <br/>
            <Typography
              style = {{
                textAlign: 'justify',
                textJustify: 'inter-word',
                flex: 2,
                fontSize: 18,
              }}
            >
              {item.details} ....
            </Typography>
            <br/>
            <div style = {styles.readMore}>
              Click to Read More
            </div>
          </div>
          <CardMedia
            component = "img"
            alt = "photo"
            style = {styles.img}
            image = {item.photoUrl}
          />
        </CardActionArea>
        <div align = 'center' style = {{padding: '5px 20px 10px 20px'}}>
          <div style = {{padding: 5, display: 'flex', color: Colors.WHITE}}>
            <RatingSlider
              min = {0}
              max = {10}
              step = {1}
              style = {{flex: 2}}
              marks
              defaultValue = {8}
              onChange = {this.handleChange}
              valueLabelDisplay="on"
            />
            <div style = {{flex: 1}}>
              <div style = {styles.ratingCircle}>
                178
              </div>
            </div>
            <div style = {{flex: 1}}>
              <div style = {styles.ratingCircle}>
                6.7
              </div>
            </div>
          </div>
          <div
            style = {{padding: 5, display: 'flex', color: Colors.WHITE}}
          >
            <div style = {{flex:2}}>
              Rate My Creativity
            </div>
            <div style = {{flex: 1}}>
              Total Ratings
            </div>
            <div style = {{flex: 1}}>
              Avg Rating
            </div>
          </div>
        </div>
      </Card>
    )
  }
  render(){
    return (
      <div
        style = {styles.container}
      >
        <Card style = {styles.photosCard}>
          <CardContent
            align = 'center'
            style = {styles.titleBar}
          >
            <div
              style = {{color: Colors.WHITE, padding: 5, flex: 3, fontSize: 25}}
            >
              My creativity list, have a look!
            </div>
          </CardContent>
          <CardContent style = {{ padding: '10px 20px', backgroundColor: Colors.WHITE}}>
            {
              this.props.photoList.list.map((item, key) =>
                this.renderPhotosList(item, key)
              )
            }
          </CardContent>
        </Card>
      </div>
    );
  }
}

const styles = {
  container: {
    flex: 7,
    paddingBottom: 10,
  },
  readMore: {
    textAlign: 'center',
    width: '70%',
    padding: 5,
    margin: '5px 10px',
    border: '2px solid black',
    borderRadius: 5,
    borderColor: Colors.WHITE,
    fontSize: 16
  },
  ratingCircle: {
    width: 40,
    height: 40,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    borderRadius: 25,
    border: `1px solid ${Colors.WHITE}`
  },
  card: {
    backgroundColor: '#21D4FD',
    backgroundImage: 'linear-gradient(19deg, #21D4FD 0%, #B721FF 100%)',
    margin: '20px 10px 0px 20px',
    borderRadius: 10,
  },
  img: {
    flex: 1,
    margin: '10px 20px 10px 0px',
    display: 'block',
    maxHeight: 300,
    maxWidth: 400
  },
  photosCard: {
    width: '97%'
  },
  caption: {
    flex: 1,
    margin: '10px 30px',
    padding: 5,
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    color: 'white',
  },
  actionAreaCard: {
    minHeight: '40vh',
    display: 'flex',
    flexDirection: 'horizontal',
  },
  titleBar: {
    padding: 5,
    display: 'flex',
    flexDirection: 'horizontal',
    backgroundColor: Colors.FOREGROUND,
  },
};

const mapStateToProps = ({ photoList }) => ({
  photoList
 });

export default connect(mapStateToProps, {

})(CreativitySection);
