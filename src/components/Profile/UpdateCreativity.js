//abhishek360
import React, { Component } from 'react';
import {
  IconButton,
  TextField,
  Button,
} from '@material-ui/core';
import { connect } from 'react-redux';
import {
  Close,
  Check
} from '@material-ui/icons';
import * as Colors from '../../configs/colors';
import '../style.css';
import {
  putUpdatePhoto,
} from '../../actions/PhotoStateActions';

class UpdateCreativity extends Component {
  state = {
    id: '',
    caption: '',
    details: '',
  }

  verifyCreativityUpdate  = (id) => {
    const {caption, details} = this.state;
    var data = {id};
    var flag = false;
    if(caption!==''){
      flag = true;
      data = {...data, caption}
    }
    if(details!==''){
      flag = true;
      data = {...data, details: details}
    }

    if(flag){
      this.props.putUpdatePhoto(data);
    }
  }

  render() {
    const {caption, details, id} = this.props.creativityDetails;
    const newCaption = this.state.caption;
    const newDetails = this.state.details;
    return (
      <div className = 'popup'>
        <div className = 'popup_inner_creativity_edit'>
          <div className = 'popup_close_creativity_add'>
            <IconButton
              onClick={this.props.closePopup}
            >
              <Close/>
            </IconButton>
          </div>
          <h2 align = "center" style = {{padding: 10, marginBottom: 20}}> Update Creativity Details</h2>
          <TextField
            style = { styles.textField }
            label = "Image Caption Here :"
            value = {newCaption==='' ? caption : newCaption}
            variant="outlined"
            onChange = {(event) =>
              this.setState({caption: event.target.value})
            }
          />
          <TextField
            style = { styles.textField }
            label = "Write Short Description Here :"
            value = {newDetails==='' ? details : newDetails}
            multiline
            rows="4"
            variant="outlined"
            onChange = {(event) => this.setState({details: event.target.value})}
          />
          <p> *Image can't be updated, add a new creativity instead. </p>
          <div
            align = 'center'
            style = {styles.footer}
          >
            <Button
              style={styles.footerButton}
            >
              <Close/> Clear
            </Button>
            <Button
              style={styles.footerButton}
              onClick={(event) => {
                  this.verifyCreativityUpdate(id);
                }
              }
            >
              <Check/> Save & Update
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  content: {
    display: 'flex',
    flexDirection: 'row',
    paddingTop: '16px',
    width: '100%',
  },
  textField: {
    width: '95%',
    margin: 10,
  },
  footer: {
    position: 'fixed',
    bottom: '25%',
    width: '60%',
    borderRadius: 5,
    background: Colors.AQUAMARINE,
  },
  footerButton: {
    marginTop: 10,
    marginBottom: 10,
    marginRight: '15px',
    color: Colors.WHITE,
    background: Colors.PURPLE,
  },
}

const mapStateToProps = ({  updateDetails }) => ({
    updateDetails
 });

export default connect(mapStateToProps, {
  putUpdatePhoto
})(UpdateCreativity);
