//abhishek360

import React from 'react';
import { connect }  from 'react-redux';
// import RequestService from '../services/RequestService';

class AuthHOC extends React.Component {
  // state = {
  //   loggedIn: false,
  // }
  //
  // constructor(){
  //   super();
  //   this.requestService = new RequestService('users','ADMIN')
  // }
  //
  // async componentDidMount() {
  //   await this.requestService.loggedIn().then((res)=> {
  //     if(res){
  //       this.setState({
  //         loggedIn: true,
  //       })
  //     }
  //   })
  // }

  render () {
    return this.props.userDetails.firstName!=='' ? this.props.yes() : this.props.no()
  }
}

AuthHOC.defaultProps = {
  loading: () => null,
  yes: () => null,
  no: () => null
};

const mapStateToProps = ({ userDetails }) => ({ userDetails });

export default connect(mapStateToProps, {

  })(AuthHOC);
