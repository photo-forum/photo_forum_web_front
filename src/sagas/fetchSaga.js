import { all, takeEvery } from 'redux-saga/effects';

import {
  FETCH_USER_DETAILS,
  FETCH_UPLOAD_URL,
  FETCH_PHOTO_LIST,
  FETCH_PHOTO_ALL,
  FETCH_USER_EVENTS
} from '../constants/action-constants';

import { fetchWorkerSaga }  from './getSagasHelpers';

export default function* fetchSagas() {
  try {
    yield all([
      takeEvery(FETCH_USER_DETAILS, fetchWorkerSaga),
      takeEvery(FETCH_UPLOAD_URL, fetchWorkerSaga),
      takeEvery(FETCH_PHOTO_LIST, fetchWorkerSaga),
      takeEvery(FETCH_PHOTO_ALL, fetchWorkerSaga),
      takeEvery(FETCH_USER_EVENTS, fetchWorkerSaga),
    ])
  }
  catch (error) {
    console.log('errors in get sagas ==> ', error);
  }
}
