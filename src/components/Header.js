//abhishek360

import React, { Component } from 'react';
import { AppBar, Toolbar } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AuthHOC from '../HOC/AuthHOC';
import { connect } from 'react-redux';
import { navigate, Link } from '@reach/router';
import NavBar from './NavBar.js'
import * as Colors from '../configs/colors'
import logo from '../assets/ourforum_logo_240.png';


class Header extends Component{
  render(){
    return(
      <div style = { styles.container } >
        <AppBar
          style = { styles.appBarContainer }
        >
          <Toolbar>
          <div align = 'center' style={styles.logoImg}>
            <img src = {logo} height= {30} width= {150} alt="OurForum"/>
          </div>
          <div style = {{flex: 5}}>
            <NavBar/>
          </div>
          <AuthHOC
            yes = {() =>
              <div align = 'center' style = {{ flex: 3 }}>
                  {
                    this.props.userDetails.email &&
                      <Typography
                        style = {{fontSize: 16, color: Colors.WHITE}}
                        variant = 'caption'
                      >
                        Hello, <Link
                                  style = {styles.linkText}
                                  to="userprofile"
                                >
                                  { this.props.userDetails.firstName.toUpperCase() }
                                </Link>
                      </Typography>
                  }
                <Button
                  id = 'headerLogoutButton'
                  style={styles.button}
                  onClick={(event) => this.props.handleLogout()}
                >
                  Logout
                </Button>
              </div>
            }
            no = {() =>
              <div align = 'center' style = {{ flex: 3 }}>
                <Button
                  id = 'headerLoginButton'
                  style={styles.button}
                  onClick={(event) => this.props.togglePopup('login')}
                >
                  Login
                </Button>
                <Button
                  id = 'headerRegisterButton'
                  style={styles.button}
                  onClick={(event) => navigate('/register')}
                >
                  Register
                </Button>
              </div>
            }
          />
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

const styles = {
  container: {
    position: 'fixed',
    top: 0,
    zIndex: 3,
    width: '100%',
  },
  linkText: {
    fontSize: 15,
    textDecoration: 'underline',
    color: Colors.SPECIAL_FONT
  },
  logoImg: {
    flex: 3,
    borderRadius: 5,
  },
  appBarContainer: {
    width: '100%',
    color: '#A4A7B2',
    display: 'flex',
    backgroundColor: Colors.SECONDARY
  },
  textField: {
    width: '20',
    margin: 10,
  },
  button: {
    marginLeft: 10,
    marginRight: 10,
    width: '20',
    color: Colors.WHITE,
    background: Colors.FOREGROUND,
  },
};

const mapStateToProps = ({ userDetails }) => ({ userDetails });

export default connect(mapStateToProps, {

  })(Header);
