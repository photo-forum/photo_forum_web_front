//abhishek360

import {
  PUT_EVENT_PARTICIPATE,
  PUT_EVENT_PARTICIPATE_SUCCESS,
  PUT_EVENT_PARTICIPATE_FAILURE,
  PUT_EVENT_PARTICIPATE_ERROR,
} from '../constants/action-constants';

const STATE = {
  message: '',
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case PUT_EVENT_PARTICIPATE:
      console.log('put event reg dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case PUT_EVENT_PARTICIPATE_SUCCESS:
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case PUT_EVENT_PARTICIPATE_FAILURE:
      console.log('put event reg failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case PUT_EVENT_PARTICIPATE_ERROR:
      console.log('put event reg error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
