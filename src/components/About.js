//abhishek360
import React, { Component } from 'react';
import * as Colors from '../configs/colors';
import {
  Grid,
  Typography,
  CardMedia,
} from '@material-ui/core';
import aboutImg from '../assets/about_img.jpg';

class About extends Component{
  imgUrl = ''
  render(){
    return (
      <div style = {styles.container}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={2}>
          </Grid>
          <Grid item xs={12} sm={5}>
            <CardMedia
              component = "img"
              alt = 'about'
              style = {{margin: 30, height: 250, width: 250 }}
              image = { aboutImg }
            />
            <Typography
              style ={styles.about}
            >
              Our Forum was founded by some photography fanatics with a roster of equally
              dedicated writers, architects and artists. Starting out in 2019, as an online
              community aimed at inspiring talents all over the world including photographers,
              architects, designers, artists and models. It offers an enticing mix of experience by
              arranging regular workshops, photo/art - walks, competitions and exhibitions.
            </Typography>
          </Grid>
          <Grid item xs={12} sm={5}>
            <div style = {{paddingTop: 100, marginLeft: '10%', width: '60%'}}>
              <div style = {styles.keyPoints}>
                <Typography style = {{fontSize: 20, margin: 5}}>
                  A global visual creative community.
                </Typography>
              </div>
              <br/>
              <div style = {styles.keyPoints}>
                <Typography style = {{fontSize: 20, margin: 5}}>
                  A marketplace for Asian Art, Architecture and Photography.
                </Typography>
              </div>
              <br/>
              <div style = {styles.keyPoints}>
                <Typography style = {{fontSize: 20, margin: 5}}>
                  A collaborations between content creators and bussiness firms.
                </Typography>
              </div>
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const styles = {
  container: {
    paddingTop: 100,
    backgroundImage: "url(https://droitthemes.com/wp/saasland-theme/wp-content/plugins/saasland-core/widgets/images/banner.png)",
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: '100vw',
    zIndex: -1,
    minHeight: '80vh'
  },
  about: {
    textAlign: 'justify',
    textJustify: 'inter-word',
    color: Colors.WHITE,
    fontSize: 22,
    width: '80%'
  },
  keyPoints: {
    boxShadow: "5px 3px 5px #9E9E9E",
    padding: 10,
    margin: 10,
    backgroundColor: Colors.WHITE,
  }
};



export default About;
