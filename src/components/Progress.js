import React, { Component } from 'react'
import './Progress.css'

class Progress extends Component {
  render() {
    console.log('progress', this.props.progress);
    return (
      <div className="ProgressBar">
        <div
          className="Progress"
          style = {{ width: this.props.progress + '%' }}
        />
      </div>
    )
  }
}

export default Progress
