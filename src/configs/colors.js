//abhishek360

export const PRIMARY = '#053C5E' //DARK IMPERIAL BLUE
export const PRIMARY_SPECIAL = '#5941A9' //PLUMP PURPLE
export const SECONDARY = '#011936'; //MAASTRICHT_BLUE
export const BACKGROUND = '#EEFFDB' //OUTER_SPACE
export const FOREGROUND = '#643173' //IMPERIAL
export const FOREGROUND_2 = '#B8F3FF' //DIAMOND
export const DARK = '#1D1E18' //EERIE BLACK
export const LIGHT = '#E3C0D3' //THISTLE
export const LIGHT_SEC = '#DEC5E3' //LANGUID LAVENDER
export const SPECIAL_FONT = '#EA638C' //LIGHT_CRIMSON
export const WHITE = '#ffffff'
export const MY_GREEN = '#345830'
export const MY_GREY = '#95a3a4'
export const MY_RED = '#81171b'

export const QUICK_SILVER = '#A4A7B2';
export const OUTER_SPACE = '#424B54';
export const MAASTRICHT_BLUE = '#011936';
export const LAPIS_LAZULI = '#2274A5';
export const IMPERIAL = '#643173';
export const AQUAMARINE = '#5DFDCB';
export const PURPLE = 'purple';
export const LIGHT_CRIMSON = '#EA638C';
export const DIAMOND = '#B8F3FF';
