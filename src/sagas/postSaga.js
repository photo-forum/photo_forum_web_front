import { all, takeEvery } from 'redux-saga/effects';

import{
  POST_CHANGE_PSSWD,
} from '../constants/action-constants';
import { postWorkerSaga } from './getSagasHelpers';

export default function* postSagas() {
  try {
    yield all([
      takeEvery(POST_CHANGE_PSSWD, postWorkerSaga),

    ])
  }
  catch (error) {
    console.log('errors in post sagas ==> ', error);
  }
}
