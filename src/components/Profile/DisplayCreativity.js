//abhishek360
import React, { Component } from 'react';
import {
  IconButton,
  Button,
} from '@material-ui/core';
import { connect } from 'react-redux';
import {
  Close,
  Delete,
  Check,
  Edit
} from '@material-ui/icons';
import * as Colors from '../../configs/colors';
import '../style.css';
import {
  putRemovePhoto,
  putUpdatePhoto,
} from '../../actions/PhotoStateActions';
import {
  renderDateTime
} from '../../helpers/UsefullFunctions';

class DisplayCreativity extends Component {
  render() {
    const { caption, details, photoUrl, createdAt, id } = this.props.creativityDetails;
    return (
      <div className = 'popup'>
        <div className = 'popup_inner_creativity_add'>
          <div className = 'popup_close_creativity_add'>
            <IconButton
              onClick={() => this.props.toggleCreativityDetails({})}
            >
              <Close/>
            </IconButton>
          </div>
          <h3> {caption} </h3>
          <div
            align = 'left'
            style = {{padding: 5, marginLeft: 15, fontSize: 12}}
          >
            {renderDateTime(createdAt)}
          </div>
          <div
            align = 'left'
            style = {{padding: 5, marginLeft: 15, marginBottom: 20, fontSize: 14}}
          >
            {details}
          </div>
          <div align = 'center' style = {styles.dropzone}>
            <img
              src = {photoUrl}
              alt=""
              style = {styles.image}
            />
          </div>
          <div
            align = 'center'
            style = {styles.footer}
          >
            <Button
              style={styles.footerButton}Check
              onClick = {() =>
                this.props.putRemovePhoto({photoId: id})
              }
            >
              <Delete/> Delete
            </Button>
            <Button
              style={styles.footerButton}
              onClick={(event) => {
                  this.props.putUpdatePhoto({id, status: 'visible'});
                }
              }
            >
              <Check/> Make Public
            </Button>
            <Button
              style={styles.footerButton}
              onClick = {() =>
                this.props.toggleUpdateCreativity()
              }
            >
              <Edit/> Edit Details
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  content: {
    display: 'flex',
    flexDirection: 'row',
    paddingTop: '16px',
    width: '100%',
  },
  dropzone: {
    height: '60%',
    width: '95%',
    marginLeft: 20,
    backgroundColor: '#fff',
    border: '2px dashed rgb(187, 186, 186)',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    maxHeight: '100%',
    borderRadius: 5,
    backgroundColor: '#fff',
    alignItems: 'center',
    objectFit: 'cover'
  },
  textField: {
    width: '95%',
    margin: 10,
  },
  footer: {
    position: 'fixed',
    bottom: '5%',
    width: '60%',
    borderRadius: 5,
    background: Colors.AQUAMARINE,
  },
  footerButton: {
    marginTop: 10,
    marginBottom: 10,
    marginRight: '15px',
    color: Colors.WHITE,
    background: Colors.PURPLE,
  },
  row: {
    borderLeft: 'thick solid #000000',
    padding: '20px',
    marginLeft: '20px',
    marginTop: '50px',
    height: '100%',
    width: '80%',
  },
  progressWrapper: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  fileName: {
    marginBottom: '8px',
    fontSize: '16px',
    color: 'blue',
  }
}

const mapStateToProps = ({ photoList }) => ({
  photoList
 });

export default connect(mapStateToProps, {
  putRemovePhoto,
  putUpdatePhoto
})(DisplayCreativity);
