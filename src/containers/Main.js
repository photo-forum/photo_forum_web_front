//abhishek360

import React, { Component } from 'react';
import Home from '../components/Home';
import About from '../components/About';
import UserProfile from './UserProfile';
import Portfolio from './Portfolio';
import SignUp from './SignUp';
import Photography from '../components/Photography';
import Arts from '../components/Arts';
import Architecture from '../components/Architecture';
import Blogs from '../components/Blogs';
import Header from '../components/Header';
import Footer from '../components/Footer';
import Popup from '../components/Popup';
import AuthHOC from '../HOC/AuthHOC';
import Loading from '../components/Loading';
import PrivacyPolicy from '../components/PrivacyPolicy';
import TermsConds from '../components/TermsConds';

import RequestService from '../services/RequestService';
import { Router, Redirect } from '@reach/router';
import { connect } from 'react-redux';
import {
  userLogout,
  fetchUserDetails,
} from '../actions/UserStateActions';
import * as Colors from '../configs/colors';


const NotFound = () => <div style = {{minHeight: '97vh', marginTop: 20}} align = 'center'>Sorry, you seems to be lost!</div>

class Main extends Component {
  state = {
    welcome: 'Welcome to Ourforum',
    showPopup: false,
    popupType: 'login'
  }

  constructor(){
    super();
    this.requestService = new RequestService('users','ADMIN')
  }

  async componentDidMount() {
    const loggedIn = await this.requestService.loggedIn();
    if(loggedIn){
      await this.props.fetchUserDetails();
    }
  }

  togglePopup = (type) => {
    this.setState({
      popupType: type,
      showPopup: !this.state.showPopup,
    })
  }

  changePopupType = (type) => {
    this.setState({
      popupType: type,
    })
  }

  handleLogin = async ( username, password ) => {
    const data = await this.requestService.auth( username, password );
    //console.log('loginnnnnnnnnnnnnnn', data);
    if(data.success){
      //alert('User Logged In!');
      this.props.fetchUserDetails();
      this.togglePopup('');
    }
    else{
      alert('Try Again, Failed to Login!');
    }
  }

  handleRegister = async ( user ) => {
    const data = await this.requestService.reg( user );
    console.log('registeredddddddddddddddddd', data);
    if(data.success){
      alert('User Registered successfully, please login to continue.');
      this.togglePopup('login');
    }
    else{
      alert('Try Again, Failed to register!');
    }
  }

  handleLogout = async () => {
    await this.requestService.logout();
  }

  render(){
    //console.log('re-render main', this.props.userDetails);
    return (
      <div style = { styles.container }>
        {
          (this.props.userDetails.isLoading || this.state.isLoading) &&
            <Loading
              isLoading={this.props.userDetails.isLoading}
              msg='Please wait, Setting up things for you!'
            />
        }
        <Header
          handleLogout = { this.handleLogout }
          togglePopup = { this.togglePopup }
        />
        {
          this.state.showPopup &&
          <Popup
            type = { this.state.popupType }
            closePopup = { this.togglePopup }
            handleLogin = { this.handleLogin }
            handleRegister = { this.handleRegister }
            changePopupType = { this.changePopupType }
          />
        }
        { !this.props.userDetails.isLoading &&
          <Router style = {{paddingTop: '3%', paddingBottom: '4%' }}>
            <Home path = '/'/>
            <About path = 'aboutus'/>
            <UserProfile path = 'userprofile'/>
            <Blogs path = 'blogs'/>
            <AuthHOC path = 'register'
              loading = {() => null}
              yes ={() =>
                <Redirect noThrow to = '/'/>
              }
              no = {() =>
                <SignUp
                  handleRegister = { this.handleRegister }
                  changePopupType = { this.changePopupType }
                />
              }
            />
            <Photography path = 'photography'/>
            <Architecture path = 'architecture'/>
            <Arts path = 'arts'/>
            <Portfolio path = 'portfolio/:portfolioId'/>
            <TermsConds path = 'terms-conditions'/>
            <PrivacyPolicy path = 'privacy-policy'/>
            <NotFound default />
          </Router >
        }
        <Footer/>
      </div>
    )
  }
}

const styles = {
  container: {
    paddingBottom: '20px',
    minHeight: '96vh',
    backgroundColor: Colors.BACKGROUND,
  },
}

const mapStateToProps = ({ userDetails }) => ({ userDetails });

export default connect(mapStateToProps, {
    userLogout,
    fetchUserDetails,
  })(Main);
