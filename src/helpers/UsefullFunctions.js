//abhishek360

export function get24hrsTo12(hours, minutes) {
  const AmOrPm = hours >= 12 ? 'PM' : 'AM';
  hours = (hours % 12) || 12;
  const time = hours + ":" + minutes + " " + AmOrPm;
  return time;
}

export function renderDateTime(jsTime) {
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                          'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  const date = new Date(jsTime);
  const time = get24hrsTo12(date.getHours(), date.getMinutes());
  const res = date.getDate()+' '+months[date.getMonth()]+', '+date.getFullYear()+' '+time;
  return res;
}

export function nth(d) {
  if (d > 3 && d < 21) return 'th';
  switch (d % 10) {
    case 1:  return "st";
    case 2:  return "nd";
    case 3:  return "rd";
    default: return "th";
  }
}

export function renderDay(jsTime) {
  const date = new Date(jsTime);
  const res = date.getDate()+nth(date.getDate());
  return res;
}

export function renderMonth(jsTime) {
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                          'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  const date = new Date(jsTime);
  const res = months[date.getMonth()];
  return res;
}

export function renderTime(jsTime) {
  const date = new Date(jsTime);
  const time = get24hrsTo12(date.getHours(), date.getMinutes());
  return time;
}

export function capitalFirst(word) {
  return word.charAt(0).toUpperCase() + word.slice(1);
}

export function generateEventState(registerAt, startsAt, endsAt) {
  const regTime = new Date(registerAt).getTime();
  const startTime = new Date(startsAt).getTime();
  const endTime = new Date(endsAt).getTime();
  const currTime = new Date().getTime();

  if(regTime>currTime){
    return 'register'
  }
  else if(regTime<currTime&&startTime>currTime){
    return 'registeration_closed'
  }
  else if(startTime<=currTime&&endTime>currTime){
    return 'ongoing'
  }
  else if(endTime<=currTime){
    return 'completed'
  }
}
