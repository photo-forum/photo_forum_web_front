//abhishek360
import React, { Component } from 'react';
import Dropzone from '../Dropzone';
import Progress from '../Progress';
import {
  CheckCircle,
} from '@material-ui/icons';

class Upload extends Component {
  renderProgress = () => {
    const uploadProgress = this.props.uploadProgress;
    const uploading = this.props.uploading;
    const successfullUploaded = this.props.successfullUploaded
    console.log('upload status',uploadProgress );
    if(uploading || successfullUploaded){
      return (
        <div style = {styles.progressWrapper}>
          <Progress progress = {uploadProgress ? uploadProgress : 0}/>
          <CheckCircle
            style = {{
              opacity:
                successfullUploaded ? 0.8 : 0
            }}
          />
        </div>
      );
    }
  }

  renderDropzone = (imgSrc) => {
    if(imgSrc&&imgSrc.length>0){
      return (
        <img
          src = {imgSrc}
          alt=""
          style = {styles.image}
        />
      )
    } else {
      return (
        <Dropzone
          onFilesAdded = {this.props.onFilesAdded}
          imgSrc = {imgSrc}
          disabled = {
            this.props.uploading || this.props.successfullUploaded
          }
        />
      )
    }
  }

  render() {
    const { file, fileName, imgSrc } = this.props;
    return (
      <div style = {styles.content}>
        <div style = {styles.dropzone}>
          {this.renderDropzone(imgSrc)}
        </div>
        {file &&
          <div style = {styles.row}>
            <div style = {styles.fileName}>{fileName}</div>
            {this.renderProgress()}
          </div>
        }
      </div>
    );
  }
}

const styles = {
  content: {
    display: 'flex',
    flexDirection: 'row',
    paddingTop: '16px',
    width: '100%',
  },
  dropzone: {
    height: '205px',
    width: '205px',
    padding: 5,
    backgroundColor: '#fff',
    border: '2px dashed rgb(187, 186, 186)',
    borderRadius: '50%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    maxHeight: '200px',
    maxWidth: '200px',
    borderRadius: '50%',
    backgroundColor: '#fff',
    alignItems: 'center',
    objectFit: 'cover'
  },
  row: {
    borderLeft: 'thick solid #000000',
    padding: '20px',
    marginLeft: '20px',
    marginTop: '50px',
    height: '100%',
    width: '80%',
  },
  progressWrapper: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  fileName: {
    marginBottom: '8px',
    fontSize: '16px',
    color: 'blue',
  }
}

export default Upload;
