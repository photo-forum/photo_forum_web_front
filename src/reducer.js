//abhishek360
import { combineReducers } from 'redux';

import UserDetailsReducer from './reducers/UserDetailsReducer';
import UpdateDetailsReducer from './reducers/UpdateDetailsReducer';
import UpdatePhotoReducer from './reducers/UpdatePhotoReducer';
import TopEventsReducer from './reducers/TopEventsReducer';
import AllEventsReducer from './reducers/AllEventsReducer';
import AllPhotosReducer from './reducers/AllPhotosReducer';
import PhotoListReducer from './reducers/PhotoListReducer';
import RegEventReducer from './reducers/RegEventReducer';
import UserEventsReducer from './reducers/UserEventsReducer';

const appReducer = combineReducers({
  userDetails: UserDetailsReducer,
  updateDetails: UpdateDetailsReducer,
  updatePhoto: UpdatePhotoReducer,
  topEvents: TopEventsReducer,
  allEvents: AllEventsReducer,
  allPhotos: AllPhotosReducer,
  photoList: PhotoListReducer,
  regEvent: RegEventReducer,
  userEvents: UserEventsReducer,
});

const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGOUT') {
    state = undefined
  }
  return appReducer(state, action)
}

export default rootReducer;
