//abhishek360

import React, { Component } from 'react';
import { connect }  from 'react-redux';
import AuthHOC from './AuthHOC';
import {
  Typography,
  Button
} from '@material-ui/core'
import * as Colors from '../configs/colors';
import {
  renderDateTime,
} from '../helpers/UsefullFunctions';

class EventStateHOC extends Component {
  renderEventState = (state) => {
    if(state==='register'){
      return (
        <div style = {styles.cardAction}>
          <Typography
            variant = 'caption'
          >
            Register Before: <br/> <b>{renderDateTime(this.props.registerAt)}</b>
          </Typography>
          <AuthHOC path = 'userprofile'
            loading = { () => null}
            yes ={ () =>
              <Button
                align = 'flex-end'
                size = 'small'
                color = 'primary'
                style = {styles.button}
              >
                Register
              </Button>
            }
            no = {() =>
              <Button
                align = 'flex-end'
                size = 'small'
                color = 'primary'
                style = {{...styles.button, opacity: 0.8}}
                onClick = {() => alert('You must Login to participate in any event.')}
              >
                Register
              </Button>
            }
          />
        </div>
      )
    }
    else if(state==='registeration_closed'){
      return (
        <div style = {{...styles.text, backgroundColor: Colors.MY_RED}}>
          Registration Closed
        </div>
      )
    }
    else if(state==='ongoing'){
      return (
        <div style = {{...styles.text, backgroundColor: Colors.MY_GREEN}}>
          On Going
        </div>
      )
    }
    else if(state==='completed'){
      return (
        <div style = {{...styles.text, backgroundColor: Colors.MY_GREY}}>
          Completed
        </div>
      )
    }
  }

  render () {
    const state = this.props.eventState;
    return (
      <div style = {styles.container}>
        {this.renderEventState(state)}
      </div>
    )
  }
}

EventStateHOC.defaultProps = {
  eventState: '',
  registerAt: () => null
};

const styles = {
  container: {
    minHeight: 40,
    width: '100%'
  },
  text: {
    fontSize: 20,
    color: Colors.WHITE,
    textAlign: 'center',
    padding: '5px 30px',
    border: '2px solid black',
    borderRadius: 5
  },
  cardAction: {
    display: 'flex',
    flexDirection: 'horizontal',
    justifyContent: 'space-between',
  },
  button: {
    padding: 5,
    margin: '5px 10px',
    color: Colors.WHITE,
    boxShadow: "5px 3px 5px #9E9E9E",
    background: Colors.FOREGROUND,
  },
};

const mapStateToProps = ({ userDetails }) => ({ userDetails });

export default connect(mapStateToProps, {

})(EventStateHOC);
