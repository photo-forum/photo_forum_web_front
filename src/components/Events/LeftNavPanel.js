//abhishek360

import React from 'react';
import {
  Card,
} from '@material-ui/core';
import * as Colors from '../../configs/colors';

class LeftNavPanel extends React.Component {
  render() {
    const { walkRef, exhiRef, competRef, workshopRef } = this.props;
    return (
      <div style = {styles.panelLeft}>
        <Card
          align = 'center'
          style = {styles.navCard}
          onClick = {() => window.scrollTo({behavior: 'smooth', top: walkRef.current.offsetTop-100})}
        >
          <img alt='Photowalks' src="https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/icons/walks.png"/>
        </Card>
        <Card
          align = 'center'
          style = {styles.navCard}
          onClick = {() => window.scrollTo({behavior: 'smooth', top: exhiRef.current.offsetTop-100})}
        >
          <img alt='Exhibitions' src="https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/icons/exhibition.png"/>
        </Card>
        <Card
          align = 'center'
          style = {styles.navCard}
          onClick = {() => window.scrollTo({behavior: 'smooth', top: competRef.current.offsetTop-100})}
        >
          <img alt='Competition' src="https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/icons/competition.png"/>
        </Card>
        <Card
          align = 'center'
          style = {{padding: 5, marginLeft: 15, flex: 1, boxShadow: "5px 3px 5px #9E9E9E", backgroundColor: Colors.DIAMOND,}}
          onClick = {() => window.scrollTo({behavior: 'smooth', top: workshopRef.current.offsetTop-100})}
        >
          <img alt='Workshops' src="https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/icons/workshop.png"/>
        </Card>
      </div>
    );
  }
}

const styles = {
  navCard: {
    backgroundColor: Colors.DIAMOND,
    boxShadow: "5px 3px 5px #9E9E9E",
    padding: 5,
    marginLeft: 15,
    marginBottom: 10,
    flex: 1
  },
  panelLeft: {
    position: 'fixed',
    left: 0,
    width: '5%',
    top: 200,
  },
  button: {
    margin: 10,
    background: 'purple',
  },
};

export default LeftNavPanel;
