//abhishek360

import React, { Component } from 'react';
import * as Colors from '../configs/colors';
import{
  Typography,
}from '@material-ui/core';
import ReactLoading from "react-loading";
import Breakpoint from 'react-socks';
import './style.css';

class Loading extends Component{

  render(){
    const { msg } = this.props;
    return (
      <div
        align = 'center'
        style = { styles.container }
      >
        <Breakpoint small down>
          <div style = {{marginTop: '50%'}} >
          <ReactLoading
            type={"bars"}
            color={"white"}
            height = {'15%'}
            width = {'15%'}
          />
          <Typography
            variant = 'caption'
            style = {{ ...styles.nameText, fontSize: 20 }}
          >
            {msg}
          </Typography>
          </div>
        </Breakpoint>
        <Breakpoint medium up>
          <div style = {{marginTop: '15%'}} >
            <ReactLoading
              type={"bars"}
              color={"white"}
              height = {'10%'}
              width = {'10%'}
            />
            <Typography
              variant = 'caption'
              style = {{ ...styles.nameText, fontSize: 25 }}
            >
              {msg}
            </Typography>
          </div>
        </Breakpoint>
      </div>
    );
  }
}

const styles = {
  container: {
    position: 'fixed',
    left: 0,
    right: 0,
    overflowY: 'hidden',
    top: 0,
    bottom: 0,
    minHeight: '100vh',
    padding: 10,
    zIndex: 5,
    alignItems: 'center',
    backgroundColor: Colors.OUTER_SPACE,
  },
  nameText: {
    color: Colors.LIGHT_CRIMSON
  },
};

export default Loading
