//abhishek360
import React, { Component } from 'react';
import {

} from '@material-ui/core';
import { connect } from 'react-redux';
import axios from 'axios';

import Popup from '../components/Popup';
import ImageCrop from '../components/Profile/ImageCrop';
import CreativityHolder from '../components/Profile/CreativityHolder';
import MyBlogsHolder from '../components/Profile/MyBlogsHolder';
import RegEventsHolder from '../components/Profile/RegEventsHolder';
import UserDetailsHolder from '../components/Profile/UserDetailsHolder';
import AddCreativity from '../components/Profile/AddCreativity';
import UpdateCreativity from '../components/Profile/UpdateCreativity';
import DisplayCreativity from '../components/Profile/DisplayCreativity';
import RequestService from '../services/RequestService';
import { requireAuth } from "../HOC/requireAuth";
import { withToastHOC } from '../HOC/ToastHOC';

import {
  fetchUploadUrl,
  resetUpdateReducer,
  fetchUserEvents
} from '../actions/UserStateActions';

import {
  fetchPhotoList
} from '../actions/PhotoStateActions';


class UserProfile extends Component{
  constructor(){
    super();
    this.adminRequests = new RequestService('users','ADMIN');
    this.photoRequests = new RequestService('photos','ADMIN');
  }

  componentDidMount(){
    this.props.fetchUserEvents(1);
    this.props.fetchPhotoList(1);
  }

  state = {
    showPopup : false,
    popupCode: 0,
    showImgCrop : false,
    showAddCreativity: false,
    showCreativityDetails: false,
    showUpdateCreativity: false,
    selectedCreativityDetails: {},
    file: '',
    fileName: '',
    imgSrc: '',
    imgDimns: {},
    uploading : false,
    uploadProgress: 0,
    successfullUploaded: false,
  }

  togglePopup = (code) => {
    this.setState({
      popupCode: code,
      showPopup: !this.state.showPopup,
      file: '',
      fileName: '',
      imgSrc: '',
      uploading : false,
      uploadProgress: 0,
      successfullUploaded: false,
    })
  }

  toggleImageCrop = () => {
    this.setState({
      showImgCrop: false,
      imgSrc: '',
      file: '',
      fileName: '',
      imgDimns: {},
    })
  }

  toggleCreativityDetails = (data) => {
    const showCreativityDetails = this.state.showCreativityDetails;
    this.setState({
      showCreativityDetails: !showCreativityDetails,
      selectedCreativityDetails: data,
    })
  }

  toggleAddCreativity = () => {
    const showAddCreativity = this.state.showAddCreativity;
    this.setState({
      showAddCreativity: !showAddCreativity,
      imgSrc: '',
      file: '',
      fileName: '',
      imgDimns: {},
    })
  }

  toggleUpdateCreativity = () => {
    const showUpdateCreativity = this.state.showUpdateCreativity;
    this.setState({
      showUpdateCreativity: !showUpdateCreativity,
    })
  }

  setCropImage = (file) => {
    console.log('file in set crop image----', file);
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = (e) => {
      this.setState({
        showImgCrop: false,
        file: file,
        imgSrc : [reader.result]
      })
    }
  }

  clearImgSelect = () => {
    this.setState({
      file: '',
      imgSrc: '',
      fileName: '',
      imgDimns: {},
      uploading : false,
      uploadProgress: 0,
      successfullUploaded: false,
    })
  }

  onFilesAdded = (file) => {
    console.log('file in profile details', file[0]);
    const { showAddCreativity } = this.state;
    var maxBytes = 3 * 1024 * 1024;
    if(showAddCreativity)
      maxBytes = 5*1024*1024;

    if(file[0].type!=='image/png' && file[0].type!=='image/jpeg')
      return;

    if(file[0].size> maxBytes)
        return;

    this.setState({
      file: file[0],
      fileName: file[0].name,
    });

    var reader = new FileReader();
    reader.readAsDataURL(file[0]);
    reader.onloadend = (e) => {
      const img = new Image();
      img.src = e.target.result;
      img.onload = () => {
        this.setState({
          showImgCrop: true,
          imgSrc : [reader.result],
          imgDimns: {
            height: img.naturalHeight,
            width: img.naturalWidth,
          }
        })
      }
    }
    reader.onerror = error => console.log(error);
  }

  uploadProPic = (file) => {
    this.setState({
      uploading: true,
    });

    this.adminRequests.get('/upload-url?key='+file.name)
    .then(res => {
      console.log('uploadProPic', res);
      this.sendToS3(res.uploadUrl, file);
    })
    .catch(e => {
      this.props.addToast(
        'Failed to upload image, Try Again!',
        {appearance: 'error'}
      );
      console.log('error in get upload url', e);
      this.setState({
        uploading: false,
      })
    });
  }

  addCreativity = (data, file) => {
    this.setState({
      uploading: true,
    });

    this.photoRequests.post(data, 'add')
    .then(res => {
      console.log('Res add new creativity', res);
      this.sendToS3(res.uploadUrl, file);
    })
    .catch(e => {
      this.props.addToast(
        'Failed to Add Creativity, Try Again!',
        {appearance: 'error'}
      );
      console.log('error in add new creativity', e);
      this.setState({
        uploading: false,
      })
    });
  }

  sendToS3 = (uploadUrl, file) => {
    const contentType = file.type;
    const options = {
      headers: {
        'Content-Type': contentType,
      },
      onUploadProgress: (progressEvent) => {
        var uploadProgress = Math.round((progressEvent.loaded * 100)/progressEvent.total);
        this.setState({
          uploadProgress,
        });
      }
    };

    if(uploadUrl.length>0){
      axios.put(uploadUrl, file, options).then(res => {
        console.log('Upload Successful', res);
        this.props.addToast(
          'Success, Image uploaded.',
          {appearance: 'success'}
        );
        this.setState({
          uploading: false,
          successfullUploaded: true,
        });
      })
      .catch(err => {
        this.props.addToast(
          'Failed to upload image, Try Again!',
          {appearance: 'error'}
        );
        this.setState({
          uploading: false
        });
        console.log('err', err);
      });
    }
  }

  handleError = (ev) => {
    ev.target.src = 'https://img.icons8.com/wired/128/000000/circled-user.png';
  }

  render(){
    return (
      <div style = {styles.container}>
        {
          this.state.showPopup &&
          <Popup
            type = 'update'
            uploading = {this.state.uploading}
            uploadProgress = {this.state.uploadProgress}
            successfullUploaded = {this.state.successfullUploaded}
            code = {this.state.popupCode}
            fileName = {this.state.fileName}
            file = {this.state.file}
            imgSrc = {this.state.imgSrc}
            clearImgSelect = {this.clearImgSelect}
            onFilesAdded = {this.onFilesAdded}
            closePopup = {this.togglePopup}
            uploadProPic = {this.uploadProPic}
            handleUpdate = { null }
          />
        }
        {this.state.showAddCreativity &&
          <AddCreativity
            file = {this.state.file}
            fileName = {this.state.fileName}
            uploading = {this.state.uploading}
            uploadProgress = {this.state.uploadProgress}
            successfullUploaded = {this.state.successfullUploaded}
            onFilesAdded = {this.onFilesAdded}
            setCropImage = {this.setCropImage}
            closePopup = {this.toggleAddCreativity}
            imgSrc = {this.state.imgSrc}
            addCreativity = {this.addCreativity}
            clearImgSelect = {this.clearImgSelect}
          />
        }
        {this.state.showImgCrop &&
          <ImageCrop
            file = {this.state.file}
            showAddCreativity = {this.state.showAddCreativity}
            showPopup = {this.state.showPopup}
            setCropImage = {this.setCropImage}
            closePopup = {this.toggleImageCrop}
            imgSrc = {this.state.imgSrc}
            imgDimns = {this.state.imgDimns}
          />
        }
        {this.state.showCreativityDetails &&
          <DisplayCreativity
            creativityDetails = {this.state.selectedCreativityDetails}
            toggleUpdateCreativity = {this.toggleUpdateCreativity}
            toggleCreativityDetails = {this.toggleCreativityDetails}
          />
        }
        {this.state.showUpdateCreativity &&
          <UpdateCreativity
            closePopup = {this.toggleUpdateCreativity}
            creativityDetails = {this.state.selectedCreativityDetails}
            toggleCreativityDetails = {this.toggleCreativityDetails}
          />
        }
        <UserDetailsHolder
          togglePopup = {this.togglePopup}
        />
        <CreativityHolder
          toggleAddCreativity = {this.toggleAddCreativity}
          handlePageChange = {(pageNo) => this.props.fetchPhotoList(pageNo)}
          toggleCreativityDetails = {this.toggleCreativityDetails}
        />
        <div style = {styles.bottomContainer}>
          <MyBlogsHolder/>
          <RegEventsHolder
            handlePageChange = {(pageNo) => this.props.fetchUserEvents(pageNo)}
          />
        </div>
      </div>
    );
  }
}

const styles = {
  container: {
    paddingTop: 40,
    width: '100vw'
  },
  photosCard: {
    minWidth: '48%'
  },
  bottomContainer: {
    marginTop: 30,
    width: '60%',
    marginLeft: '20%',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'horizontal',
  },
};

const mapStateToProps = ({ userDetails, updateDetails }) => ({
   userDetails, updateDetails
 });

export default connect(mapStateToProps, {
  fetchUserEvents,
  fetchUploadUrl,
  resetUpdateReducer,
  fetchPhotoList
})(requireAuth(withToastHOC(UserProfile)));
