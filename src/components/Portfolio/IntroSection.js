//abhishek360

import React, { Component } from 'react';
import * as Colors from '../../configs/colors';
import {
  Typography,
  Card,
  CardContent,
} from '@material-ui/core';
import { connect } from 'react-redux';
import {
} from '@material-ui/icons';
import {
  capitalFirst,
} from '../../helpers/UsefullFunctions';

class IntroSection extends Component{
  render(){
    const user = this.props.userDetails;
    return (
      <div style = {styles.container}>
        <Card style = {styles.detailsCard}>
          <CardContent style = {styles.cardContentTop}>
            <div style = {{flex: 1}}>
              <img
                onError = {this.handleError}
                alt = ''
                style = {styles.userImg}
                src = {user.proPic}
              />
            </div>
            <div style = {{ margin: 10, padding: 10, flex: 3}}>
              <Typography
                style = {{fontSize: 35, color: 'white'}}
                variant = 'caption'
              >
                {capitalFirst(user.firstName)+" "+capitalFirst(user.lastName)}
              </Typography>
              <br/>
              <Typography
                style = {{fontSize: 18, color: 'white'}}
                variant = 'caption'
              >
                @{user.username}
              </Typography>
              <br/>
              <Typography
                gutterBottom
                style = {{fontSize: 20, color: 'white'}}
                variant = 'caption'
              >
                I'm Interested in <b>{capitalFirst(user.interest)}.</b>
              </Typography>
              <br/>
              <Typography
                gutterBottom
                style = {{fontSize: 20, color: 'white'}}
                variant = 'caption'
              >
                {user.bio===''? 'User Short Bio': user.bio}
              </Typography>
            </div>
          </CardContent>
        </Card>
      </div>
    );
  }
}

const styles = {
  container: {

  },
  userImg: {
    margin: 10,
    backgroundColor: Colors.WHITE,
    border: '3px solid black' ,
    borderRadius: 40,
    height: 240,
    width: 240
  },
  detailsCard: {
    marginLeft: '15%',
    width: '70%'
  },
  cardContentTop: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'horizontal',
    backgroundImage: 'linear-gradient(to top, #f0c27b, #4b1248)',
  },
};

const mapStateToProps = ({ userDetails }) => ({
  userDetails
 });

export default connect(mapStateToProps, {

})(IntroSection);
