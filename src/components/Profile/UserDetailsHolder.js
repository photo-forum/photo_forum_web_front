//abhishek360

import React, { Component } from 'react';
import * as Colors from '../../configs/colors';
import {  Link } from "@reach/router";
import {
  Grid,
  Typography,
  Card,
  CardContent,
  IconButton,
  Button,
} from '@material-ui/core';
import { connect } from 'react-redux';
import {
  Edit,
} from '@material-ui/icons';
import {
  capitalFirst,
} from '../../helpers/UsefullFunctions';

class UserDetailsHolder extends Component{
  render(){
    const user = this.props.userDetails;
    return (
      <div
        style = {styles.container}
      >
        <Card style = {styles.detailsCard}>
            <CardContent align = 'center' style = {styles.cardContentTop}>
              <div align = 'right'>
              <Link
                getProps = {({isCurrent} ) =>{
                  return {
                    style:{
                      fontSize: 18,
                      color: Colors.WHITE,
                    }
                  }
                }}
                title = "My Public Portfolio"
                to = {`/portfolio/${user.id}`}
              >
                @{user.username}
              </Link>
              <IconButton
                onClick = {()=>this.props.togglePopup(6)}
                size = 'small'
                style = {styles.iconButton}
                edge="end"
                aria-label="delete"
              >
                <Edit/>
              </IconButton>
              </div>
              <img
                onError = {this.handleError}
                alt = ''
                style = {styles.img}
                src = {user.proPic}
              />
              <br/>
              <Typography
                gutterBottom
                style = {{fontSize: 35, color: 'white'}}
                variant = 'caption'
              >
                {user.firstName+" "+user.lastName}
              </Typography>
              <IconButton
                onClick = {()=>this.props.togglePopup(0)}
                size = 'small'
                style = {styles.iconButton}
                edge="end"
                aria-label="delete"

              >
                <Edit/>
              </IconButton>
              <br/>
              <Typography
                gutterBottom
                style = {{fontSize: 20, color: 'white'}}
                variant = 'caption'
              >
                {user.bio===''? 'User Short Bio': user.bio}
              </Typography>
              <IconButton
                onClick = {()=>this.props.togglePopup(1)}
                size = 'small'
                style = {styles.iconButton}
                edge="end"
                aria-label="delete"
              >
                <Edit/>
              </IconButton>
            </CardContent>
            <CardContent style = {styles.cardContent}>
              <Grid container spacing={2}>
                <Grid item xs = {7}>
                  <Typography
                    gutterBottom
                    style = {{fontSize: 20,}}
                    variant = 'caption'
                  >
                    My Interest: {capitalFirst(user.interest)}
                  </Typography>
                  <IconButton
                    onClick = {()=>this.props.togglePopup(3)}
                    size = 'small'
                    style = {styles.iconButton}
                    edge="end"
                    aria-label="delete"
                  >
                    <Edit/>
                  </IconButton>
                  <br/>
                  <Typography
                    gutterBottom
                    variant = 'caption'
                    style = {{fontSize: 16,}}
                  >
                    Email: {user.email}
                  </Typography>
                  <br/>
                  <Typography
                    gutterBottom
                    variant = 'caption'
                    style = {{fontSize: 16,}}
                  >
                    Phone: {user.phone}
                  </Typography>
                  <br/>
                  <Typography
                    gutterBottom
                    style = {{fontSize: 16,}}
                    variant = 'caption'
                  >
                     Gender: {user.gender==='m' ? 'Male' : 'Female' }
                  </Typography>
                  <IconButton
                    onClick = {()=>this.props.togglePopup(2)}
                    size = 'small'
                    style = {styles.iconButton}
                    edge="end"
                    aria-label="delete"
                  >
                    <Edit/>
                  </IconButton>
                  <br/>
                  <Typography
                    gutterBottom
                    style = {{fontSize: 16,}}
                    variant = 'caption'
                  >
                     Date of Birth: {user.dob }
                  </Typography>
                </Grid>
                <Grid align = 'right' item xs={5}>
                  <Button
                    id = 'changePassword'
                    style={styles.button}
                    size = 'small'
                    onClick = {()=>this.props.togglePopup(4)}
                  >
                    Change Password
                  </Button>
                  <Button
                    id = 'changeProPic'
                    style={styles.button}
                    size = 'small'
                    onClick = {()=>this.props.togglePopup(5)}
                  >
                    Change Profile Image
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
        </Card>
      </div>
    );
  }
}

const styles = {
  container: {

  },
  detailsCard: {
    marginLeft: '20%',
    width: '60%'
  },
  img: {
    backgroundColor: 'white',
    border: '4px solid blue' ,
    borderRadius: 100,
    minHeight: 200,
    minWidth: 200,
    height: 200,
    width: 200
  },
  cardContentTop: {
    alignItems: 'center',
    backgroundImage: 'linear-gradient(to top, #f0c27b, #4b1248)',
  },
  cardContent: {
    backgroundColor: Colors.FOREGROUND_2,
  },
  cardAction: {
    minHeight: 30,
    backgroundColor: Colors.AQUAMARINE,
    justifyContent: 'center',
  },
  button: {
    padding: 5,
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    color: Colors.WHITE,
    background: Colors.FOREGROUND,
  },
  iconButton: {
    marginLeft: 20,
    backgroundColor: Colors.QUICK_SILVER,
  },
};

const mapStateToProps = ({ userDetails }) => ({
  userDetails
 });

export default connect(mapStateToProps, {

})(UserDetailsHolder);
