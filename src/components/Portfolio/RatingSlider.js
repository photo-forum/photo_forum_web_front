//abhishek360

import {
  Slider,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import * as Colors from '../../configs/colors';


export const RatingSlider = withStyles({
  root: {
    color: Colors.FOREGROUND,
    height: 10,
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 4px)',
  },
  track: {
    height: 10,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
})(Slider);
