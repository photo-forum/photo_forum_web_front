//abhishek360

import {
  POST_CHANGE_PSSWD,
  POST_CHANGE_PSSWD_SUCCESS,
  POST_CHANGE_PSSWD_FAILURE,
  POST_CHANGE_PSSWD_ERROR,
  PUT_DETAILS_UPDATE,
  PUT_DETAILS_UPDATE_SUCCESS,
  PUT_DETAILS_UPDATE_FAILURE,
  PUT_DETAILS_UPDATE_ERROR,
  FETCH_UPLOAD_URL,
  FETCH_UPLOAD_URL_SUCCESS,
  FETCH_UPLOAD_URL_FAILURE,
  FETCH_UPLOAD_URL_ERROR,
  PUT_USERNAME_UPDATE,
  PUT_USERNAME_UPDATE_SUCCESS,
  PUT_USERNAME_UPDATE_FAILURE,
  PUT_USERNAME_UPDATE_ERROR,
} from '../constants/action-constants';

const STATE = {
  message: '',
  uploadUrl: '',
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case POST_CHANGE_PSSWD:
      console.log('post change password dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case POST_CHANGE_PSSWD_SUCCESS:
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case POST_CHANGE_PSSWD_FAILURE:
      console.log('post change password failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case POST_CHANGE_PSSWD_ERROR:
      console.log('post change password error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    case PUT_DETAILS_UPDATE:
      console.log('put update details dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case PUT_DETAILS_UPDATE_SUCCESS:
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case PUT_DETAILS_UPDATE_FAILURE:
      console.log('put update details failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case PUT_DETAILS_UPDATE_ERROR:
      console.log('put update details error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    case FETCH_UPLOAD_URL:
      console.log('get upload url dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case FETCH_UPLOAD_URL_SUCCESS:
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case FETCH_UPLOAD_URL_FAILURE:
      console.log('put update details failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case FETCH_UPLOAD_URL_ERROR:
      console.log('put update details error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

      case PUT_USERNAME_UPDATE:
        console.log('put username update dispatched');
        return {
          ...state,
          ...STATE,
          isLoading: true,
        }

      case PUT_USERNAME_UPDATE_SUCCESS:
        return {
          ...state,
          ...action.payload,
          isLoading: action.isLoading,
        };

      case PUT_USERNAME_UPDATE_FAILURE:
        console.log('put username update failure', action);
        return {
          ...state,
          ...action.error,
          status: 'failed',
          isLoading: action.isLoading,
        }

      case PUT_USERNAME_UPDATE_ERROR:
        console.log('put username update error',action);
        return {
          ...state,
          ...action.error,
          status: 'error',
          isLoading: action.isLoading,
        }

    default :
      return state;
  }
};
