import { all, takeEvery } from 'redux-saga/effects';
import {
  PUT_DETAILS_UPDATE,
  PUT_LIST_ALL,
  PUT_LIST_TOP,
  PUT_UPDATE_PHOTO,
  PUT_REMOVE_PHOTO,
  PUT_EVENT_PARTICIPATE,
  PUT_USERNAME_UPDATE
} from '../constants/action-constants';

import { putWorkerSaga } from './getSagasHelpers';

export default function* putSagas() {
  try {
    yield all([
      takeEvery(PUT_DETAILS_UPDATE, putWorkerSaga),
      takeEvery(PUT_LIST_ALL, putWorkerSaga),
      takeEvery(PUT_LIST_TOP, putWorkerSaga),
      takeEvery(PUT_UPDATE_PHOTO, putWorkerSaga),
      takeEvery(PUT_REMOVE_PHOTO, putWorkerSaga),
      takeEvery(PUT_EVENT_PARTICIPATE, putWorkerSaga),
      takeEvery(PUT_USERNAME_UPDATE, putWorkerSaga),
    ])
  }
  catch (error) {
    console.log('errors in put sagas ==> ', error);
  }
}
