//abhishek360

import {
  PUT_UPDATE_PHOTO,
  PUT_UPDATE_PHOTO_SUCCESS,
  PUT_UPDATE_PHOTO_FAILURE,
  PUT_UPDATE_PHOTO_ERROR,
  PUT_REMOVE_PHOTO,
  PUT_REMOVE_PHOTO_SUCCESS,
  PUT_REMOVE_PHOTO_FAILURE,
  PUT_REMOVE_PHOTO_ERROR,
  FETCH_UPLOAD_URL,
  FETCH_UPLOAD_URL_SUCCESS,
  FETCH_UPLOAD_URL_FAILURE,
  FETCH_UPLOAD_URL_ERROR,

} from '../constants/action-constants';

const STATE = {
  message: '',
  uploadUrl: '',
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case PUT_UPDATE_PHOTO:
      console.log('put update photo dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case PUT_UPDATE_PHOTO_SUCCESS:
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case PUT_UPDATE_PHOTO_FAILURE:
      console.log('put update photo failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case PUT_UPDATE_PHOTO_ERROR:
      console.log('put update photo error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    case PUT_REMOVE_PHOTO:
      console.log('put remove photo dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case PUT_REMOVE_PHOTO_SUCCESS:
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case PUT_REMOVE_PHOTO_FAILURE:
      console.log('put remove photo failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case PUT_REMOVE_PHOTO_ERROR:
      console.log('put remove photo error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    case FETCH_UPLOAD_URL:
      console.log('get upload url dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case FETCH_UPLOAD_URL_SUCCESS:
      console.log('put update details failure');
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case FETCH_UPLOAD_URL_FAILURE:
      console.log('put update details failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case FETCH_UPLOAD_URL_ERROR:
      console.log('put update details error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
