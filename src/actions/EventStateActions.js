import {
  ADMIN,
  PUT_LIST_TOP,
  PUT_LIST_ALL,
  PUT_EVENT_PARTICIPATE
} from '../constants/action-constants';

export const putEventParticipate = (eventId)=>({
  type : PUT_EVENT_PARTICIPATE,
  route : `events/participate`,
  domain : ADMIN,
  data :{
    eventId
  }
})

export const putListTop = (category)=>({
  type : PUT_LIST_TOP,
  route : `events/list-top`,
  domain : ADMIN,
  data :{
    category
  }
})

export const putListAll = (category, subCategory)=>({
  type : PUT_LIST_ALL,
  route : `events/list-all`,
  domain : ADMIN,
  data :{
    category,
    subCategory,
  }
})
