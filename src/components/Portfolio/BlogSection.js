//abhishek360

import React, { Component } from 'react';
import * as Colors from '../../configs/colors';
import {
  Grid,
  Typography,
  Card,
  CardContent,
  CardActionArea,
  CardMedia,
} from '@material-ui/core';
import { connect } from 'react-redux';
import {

} from '@material-ui/icons';
import Pagination from '../Pagination';

class BlogSection extends Component{
  renderBlogList = (item) => {
    return (
      <Grid
        item lg = {3}
        key = { item.id }
      >
      <Card style = {{ margin: 5,}}>
        <CardActionArea
          align = 'center'
          style = { styles.actionAreaCard }
        >
          <div
            style = {styles.caption}
          >
            <Typography
              style = {{fontSize: 14}}
            >
              { item.caption }
            </Typography>
            <Typography
              style = {{fontSize: 10}}
            >
              { item.createdAt }
            </Typography>
          </div>
          <CardMedia
            component = "img"
            alt = "photo"
            style = {{display: 'block', margin: 'auto', maxHeight: 180, maxWidth: 240}}
            image = {item.photoUrl}
          />
        </CardActionArea>
      </Card>
    </Grid>
    )
  }
  render(){
    return (
      <div style = {styles.container}>
        <Card>
            <CardContent
              align = 'center'
              style = {styles.titleBar}
            >
              <div
                style = {{ color: Colors.WHITE, padding: 5, flex: 3, fontSize: 25}}
              >
                My Blogs
              </div>
              <div
                style = {{color: Colors.WHITE, padding: 5, flex: 0.5, fontSize: 20}}
              >
                Pages:
              </div>
              <Pagination
                totalRecords = {this.props.photoList.count}
                onPageChanged = {(pageData) =>
                  this.props.handlePageChange(pageData.currentPage)
                }
              />
            </CardContent>
            <CardContent style = {{padding: 5, backgroundColor: Colors.WHITE}}>
              <Card style = {{backgroundColor: Colors.AQUAMARINE, margin: 5, padding: 5}}>
                <CardContent
                  style = {{padding: 5, ...styles.actionAreaCard}}
                >
                  <div align = 'center'>
                    <Typography
                      style = {{fontSize: 25}}
                    >
                      No Blogs Published
                    </Typography>
                  </div>
                </CardContent>
              </Card>
                {
                  // this.props.photoList.list.map(item =>
                  //   this.renderBlogList(item)
                  // )
                }
            </CardContent>
        </Card>
      </div>
    );
  }
}

const styles = {
  container: {
    flex: 3,
  },
  caption: {
    padding: 5,
    width: '100%',
    color: 'white',
    bottom: 0,
    backgroundColor: "rgba(0,0,0, 0.6)",
    position: 'absolute'
  },
  actionAreaCard: {
    maxHeight: 100,
    alignItems: 'center',
    justifyContent: 'center',
    border: '2px solid black'
  },
  titleBar: {
    padding: 5,
    display: 'flex',
    flexDirection: 'horizontal',
    backgroundColor: Colors.FOREGROUND,
  },
  button: {
    marginRight: 20,
    color: 'white',
    background: 'purple',
  },
};

const mapStateToProps = ({ photoList }) => ({
  photoList
 });

export default connect(mapStateToProps, {

})(BlogSection);
