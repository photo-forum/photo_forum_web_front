//abhishek360

import React, { Component } from 'react';
import * as Colors from '../../configs/colors';
import {
  Typography,
  Card,
  CardContent,
  CardActionArea,
  TextField,
} from '@material-ui/core';
import { connect } from 'react-redux';
import {

} from '@material-ui/icons';
import Pagination from '../Pagination';
import {
  renderTime,
  renderDay,
  renderMonth,
  renderDateTime,
  capitalFirst
} from '../../helpers/UsefullFunctions';
// of {capitalFirst(category)}
class RegEventsHolder extends Component{
  renderRegEventList = (item) => {
    const { title, startsAt, category, subCategory } = item.event;
    const { eventId, createdAt } = item;
    return (
      <Card key style = {{ margin: 5, boxShadow: "5px 3px 5px #9E9E9E",}}>
        <CardActionArea
          align = 'center'
          style = {styles.actionAreaCard}
        >
          <div style = {{display: 'flex', flexDirection: 'horizontal'}}>
            <div align = 'center'
              style = {styles.cardLeftSection}
            >
              <Typography
                style = {{color: Colors.WHITE, fontSize: 22}}
              >
                {subCategory.toUpperCase()}
              </Typography>
              <Typography
                style = {{color: Colors.WHITE, fontSize: 20}}
              >
                {renderDay(startsAt)}
              </Typography>
              <Typography
                style = {{color: Colors.WHITE, fontSize: 15}}
              >
                {renderMonth(startsAt)}
              </Typography>
              <Typography
                style = {{color: Colors.WHITE, fontSize: 18}}
              >
                {renderTime(startsAt)}
              </Typography>
            </div>
            <div style = {{border: `2px solid ${Colors.FOREGROUND}`, flex: 2, padding: 5}}>
              <Typography
                style = {{fontSize: 25}}
              >
                {title}
              </Typography>
              <Typography
                style = {{fontSize: 16, marginBottom: 5}}
              >
                Category: {capitalFirst(category)}
              </Typography>
              <div align = 'center'>
                <TextField
                  id = 'registered_at'
                  variant = "outlined"
                  disabled
                  label = "Registered On:"
                  name = "phone"
                  value = {renderDateTime(createdAt)}
                  inputProps = {{
                    style: {
                      textAlign: 'center',
                      padding: 8,
                      fontSize: 18,
                      color: Colors.MY_RED
                    }
                  }}
                  InputLabelProps={{
                    style: {
                      marginLeft: '65px',
                      fontSize: 18,
                      color: Colors.MY_RED
                    },
                  }}
                />
              </div>
            </div>
          </div>
        </CardActionArea>
      </Card>
    )
  }

  renderNoEventCard = (count) => {
    if(count===0){
      return (
        <Card
          style = {{backgroundColor: Colors.AQUAMARINE, margin: 5, padding: 5}}
        >
          <div
            style = {{ ...styles.actionAreaCard}}
          >
            <div align = 'center'>
              <Typography
                style = {{fontSize: 25}}
              >
                Yet to Participate.
              </Typography>
            </div>
          </div>
        </Card>
      )
    }
  }
  render(){
    const {count, rows} = this.props.userEvents;
    console.log('userEvents', this.props.userEvents);
    return (
      <div style = {styles.container}>
        <Card>
          <CardContent
            align = 'center'
            style = {styles.header}
          >
            <div
              style = {{ padding: 5, flex: 3, fontSize: 22}}
            >
              Registred Events
            </div>
            <div
              style = {{ padding: 5, flex: 0.5, fontSize: 22}}
            >
              Pages:
            </div>
            <Pagination
              totalRecords = {this.props.userEvents.count}
              onPageChanged = {(pageData) =>
                this.props.handlePageChange(pageData.currentPage)
              }
            />
          </CardContent>
          <CardContent style = {{ padding: 5, backgroundColor: Colors.WHITE}}>
              {
                count===0 ? this.renderNoEventCard(count) : rows.map((item, key) =>
                  this.renderRegEventList(item, key)
                )
              }
          </CardContent>
        </Card>
      </div>
    );
  }
}

const styles = {
  container: {
    minWidth: '48%'
  },
  cardLeftSection: {
    flex: 1,
    backgroundColor: Colors.FOREGROUND,
    paddingLeft: 10,
    paddingBottom: 5
  },
  header: {
    padding: 5,
    display: 'flex',
    flexDirection: 'horizontal',
    backgroundColor: Colors.FOREGROUND_2,
  },
  actionAreaCard: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

const mapStateToProps = ({ userEvents }) => ({
  userEvents
 });

export default connect(mapStateToProps, {

})(RegEventsHolder);
