import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import { Close  } from '@material-ui/icons';
import Login from './Login';
import UpdateDetails from './Profile/UpdateDetails';
import './style.css';

class Popup extends React.Component {
  render() {
    return (
      <div className = 'popup'>
        <div className = 'popup_inner'>
        <div className = 'popup_close'>
          <IconButton
            onClick={this.props.closePopup}
          >
            <Close/>
          </IconButton>
        </div>
        {
          (this.props.type === 'login') &&
            <Login
              closePopup = {this.props.closePopup}
              handleLogin = { this.props.handleLogin }
              changePopupType = { this.props.changePopupType }
            />
        }
        {
          (this.props.type === 'update') &&
            <UpdateDetails
              uploadProPic = {this.props.uploadProPic}
              onFilesAdded = {this.props.onFilesAdded}
              uploading = {this.props.uploading}
              uploadProgress = {this.props.uploadProgress}
              successfullUploaded = {this.props.successfullUploaded}
              file = {this.props.file}
              fileName = {this.props.fileName}
              imgSrc = {this.props.imgSrc}
              updateCode = {this.props.code}
              clearImgSelect = {this.props.clearImgSelect}
              closePopup = {this.props.closePopup}
              handleUpdate = { this.props.handleUpdate }
            />
        }
        </div>
      </div>
    );
  }
}


export default Popup;
