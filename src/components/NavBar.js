//abhishek360

import React, { Component } from 'react';
import {  Link } from "@reach/router";
import{
  List,
  ListItem,
  ListItemText,
} from '@material-ui/core';
import * as Colors from '../configs/colors'


class NavBar extends Component {
  render(){
    return (
      <List component = 'nav'>
        <ListItem component = 'div'>
          <ListItemText inset>
            <Link getProps = {({isCurrent} ) =>{
              return {
                style:{
                  ...styles.linkText,
                  color: isCurrent ? Colors.SPECIAL_FONT : Colors.WHITE,
                }
              }
            }}

            to="/"
            >
              Home
            </Link>
          </ListItemText>
          <ListItemText inset>
            <Link to="photography"
              getProps = {({isCurrent} ) =>{
                return {
                  style:{
                    ...styles.linkText,
                    color: isCurrent ? Colors.SPECIAL_FONT : Colors.WHITE,
                  }
                }
              }}
            >
              Photography
            </Link>
            </ListItemText>
          <ListItemText inset>
            <Link to="architecture"
              getProps = {({isCurrent} ) =>{
                return {
                  style:{
                    ...styles.linkText,
                    color: isCurrent ? Colors.SPECIAL_FONT : Colors.WHITE,
                  }
                }
              }}
            >
              Architecture
            </Link>
          </ListItemText>
          <ListItemText inset>
            <Link to="arts"
              getProps = {({isCurrent} ) =>{
                return {
                  style:{
                    ...styles.linkText,
                    color: isCurrent ? Colors.SPECIAL_FONT : Colors.WHITE,
                  }
                }
              }}
            >
              Arts
            </Link>
          </ListItemText>
          <ListItemText inset>
            <Link to="blogs"
              getProps = {({isCurrent} ) =>{
                return {
                  style:{
                    ...styles.linkText,
                    color: isCurrent ? Colors.SPECIAL_FONT : Colors.WHITE,
                  }
                }
              }}
            >
              Blogs
            </Link>
          </ListItemText>
        </ListItem>
      </List>
    )
  }
}

const styles = {
  linkText: {
    textDecoration: 'none',
  },
}

export default NavBar;
