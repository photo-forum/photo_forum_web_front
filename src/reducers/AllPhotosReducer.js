//abhishek360

const STATE = {
  list: [],
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case 'FETCH_PHOTO_ALL':
      console.log('get photo all dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case 'FETCH_PHOTO_ALL_SUCCESS':
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case 'FETCH_PHOTO_ALL_FAILURE':
      console.log('get photo all failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case 'FETCH_PHOTO_ALL_ERROR':
      console.log('get photo all error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
