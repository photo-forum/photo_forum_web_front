//abhishek360

import React from 'react';
import { connect } from 'react-redux';
import {
  photoSlides,
  category,
} from '../configs/content';
import EventCategory from './Events/EventCategory';
import {

} from '@material-ui/core';
import AboutCard from './Events/AboutCard';
import LeftNavPanel from './Events/LeftNavPanel';
import Slider from 'react-slick';
import * as Colors from '../configs/colors';
import {
  putListTop
} from '../actions/EventStateActions';

class Photography extends React.Component {
  constructor(props) {
    super(props);
    this.walkRef = React.createRef();
    this.exhiRef = React.createRef();
    this.competRef = React.createRef();
    this.workshopRef = React.createRef();
  }

  componentDidMount(){
    this.props.putListTop('photography');
  }

  render() {
    console.log('evnts list', this.props.topEvents);
    const {photowalks, exhibitions, workshops, competitions} = this.props.topEvents;
    return (
      <div style = { styles.container }>
        <div style = {styles.slider}>
          <Slider {...settings}>
            {
              photoSlides.map(item => {
                return (
                    <img key = {item.key} src={item.img} height= {720} width= {1080} alt="Photography"/>
                )
              })
            }
          </Slider>
        </div>
        <LeftNavPanel
          walkRef = {this.walkRef}
          exhiRef = {this.exhiRef}
          competRef = {this.competRef}
          workshopRef = {this.workshopRef}
        />
        <AboutCard
          categoryDetails = {category[0]}
        />
        <div ref = {this.walkRef}>
          <EventCategory
            categoryTitle = 'PHOTOWALKS:'
            categoryEvents = {photowalks}

          />
        </div>
        <div ref = {this.exhiRef}>
          <EventCategory
            categoryTitle = 'EXHIBITIONS:'
            categoryEvents = {exhibitions}

          />
        </div>
        <div ref = {this.competRef}>
          <EventCategory
            categoryTitle = 'COMPETITIONS:'
            categoryEvents = {competitions}

          />
        </div>
        <div ref = {this.workshopRef}>
          <EventCategory
            categoryTitle = 'WORKSHOPS:'
            categoryEvents = {workshops}

          />
        </div>
      </div>
    );
  }
}

const settings = {
  adaptiveHeight: true,
  infinite: true,
  speed: 400,
  autoplay: true,
  slidesToShow: 1,
  slidesToScroll: 1
};

const styles = {
  container: {
    backgroundColor: Colors.BACKGROUND,
    width: '100vw'
  },
  slider: {
    marginTop: 30,
    marginBottom: 30,
    marginLeft: '20%',
    width: '60%',
    boxShadow: "5px 3px 5px #9E9E9E"
  },
  button: {
    margin: 10,
    background: 'purple',
  },
};

const mapStateToProps = ({ topEvents }) => ({ topEvents });

export default connect(mapStateToProps, {
    putListTop,
  })(Photography);
