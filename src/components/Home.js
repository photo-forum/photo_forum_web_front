//abhishek360

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Grid,
  Card,
  CardActionArea,
  CardMedia,
} from '@material-ui/core'
import {
  fetchUserDetails,
} from '../actions/UserStateActions';
import { navigate } from '@reach/router';
import {
  category,
  homeSlide
} from '../configs/content';
import Slider from 'react-slick';
import * as Colors from '../configs/colors';

class Home extends Component {
  render(){
    return (
      <div style = {styles.container}>
        <div style = {styles.slider}>
          <Slider {...settings}>
            {
              homeSlide.map(item => {
                return (
                    <img key = {item.key} src = {item.img} height = {480} width = {1080} alt=""/>
                )
              })
            }
          </Slider>
        </div>
        <div style = {{paddingTop: 20, paddingBottom: 10, marginLeft: '20%', marginRight: '20%', width: '60%'}}>
          <Grid
            container
            direction = 'row'
            justify = 'space-between'
            alignItems = 'flex-end'
          >
            {
              category.map(item => {
                return (
                  <Grid
                    item xs = {2}
                    key = { item.title }
                    onClick = {() => navigate(`${item.value}`)}
                  >
                  <Card style = {styles.card}>
                    <CardActionArea style = { styles.actionAreaCard }>
                      <CardMedia
                        component = "img"
                        alt = {item.title}
                        style = {{ height: '100%', width: '100%' }}
                        image = { item.img }
                        tittle = {item.title}
                      />
                    </CardActionArea>
                  </Card>
                </Grid>
                )
              })
            }
          </Grid>
        </div>
      </div>
    );
  }
}

const settings = {
  adaptiveHeight: true,
  infinite: true,
  speed: 400,
  autoplay: true,
  slidesToShow: 1,
  slidesToScroll: 1
};

const styles = {
  container: {
    paddingTop: 40,
  },
  slider:{
    boxShadow: "5px 3px 5px #9E9E9E",
    marginLeft: '20%',
    width: '60%'
  },
  card: {
    margin: 0,
    backgroundColor: Colors.DIAMOND,
    boxShadow: "5px 3px 5px #9E9E9E",
  },
  actionAreaCard: {
  },

};

const mapStateToProps = ({ userDetails }) => ({ userDetails });

export default connect(mapStateToProps, {
    fetchUserDetails,
  })(Home);
