//abhishek360

const STATE = {
  count: 0,
  list: [],
  isLoading: false,
};

export default (state = STATE, action) => {
  switch (action.type) {
    case 'FETCH_PHOTO_LIST':
      console.log('get photo list dispatched');
      return {
        ...state,
        ...STATE,
        isLoading: true,
      }

    case 'FETCH_PHOTO_LIST_SUCCESS':
      return {
        ...state,
        ...action.payload,
        isLoading: action.isLoading,
      };

    case 'FETCH_PHOTO_LIST_FAILURE':
      console.log('get photo list failure');
      return {
        ...state,
        ...action.error,
        status: 'failed',
        isLoading: action.isLoading,
      }

    case 'FETCH_PHOTO_LIST_ERROR':
      console.log('get photo list error',action);
      return {
        ...state,
        ...action.error,
        status: 'error',
        isLoading: action.isLoading,
      }

    default :
      return state;
  }
};
