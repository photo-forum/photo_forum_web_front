//abhishek360

import React, { Component } from 'react';
import {
  CloudUploadTwoTone,
} from '@material-ui/icons';

class Dropzone extends Component {
  constructor(props) {
    super(props);
    this.fileInputRef = React.createRef();
    this.state = {
      highlight: false,
    }
  }

  openFilesDailog = () => {
    if(this.props.disabled)
      return;

    this.fileInputRef.current.click();
  }

  onFilesAdded = (event) => {
    if(this.props.disabled)
      return;

    const files = event.target.files;
    if(this.props.onFilesAdded){
      const array = this.fileListToArray(files);
      this.props.onFilesAdded(array);
    }
  }

  fileListToArray(list) {
    const array = [];
    for (var i = 0; i<list.length; i++){
      array.push(list.item(i));
    }
    return array;
  }

  onDragOver = (event) => {
    event.preventDefault();

    if(this.props.disabled)
      return;

    this.setState({highlight : true})
  }

  onDragLeave = () => {
    this.setState({ hightlight: false });
  }

  onDrop =  (event) => {
    event.preventDefault();

    if(this.props.disabled)
      return;

    const files = event.dataTransfer.files;
    if(this.props.onFilesAdded){
      const array = this.fileListToArray(files);
      this.props.onFilesAdded(array);
    }
    this.setState({highlight: false});
  }

  renderCropImage = (creativity) => {
    return (
      <div
        onDragOver = {this.onDragOver}
        onDragLeave = {this.onDragLeave}
        onDrop = {this.onDrop}
        onClick = {this.openFilesDailog}
        style = {{ ...styles.dropzone,
          backgroundColor: this.state.highlight ? 'rgb(188, 185, 236)' : '#fff',
          cursor: this.props.disabled ? "default" : "pointer"
        }}
      >
        <CloudUploadTwoTone/>
        <input
          ref = {this.fileInputRef}
          style = {styles.fileInput}
          type = "file"
          accept = "image/png, image/jpeg"
          onChange = {this.onFilesAdded}
        />
        <span>Drop Image</span>
        <span>Or</span>
        <span>Click to select</span>
        <br/>
        {
          creativity&&
          <span>*Only .jpeg or .png file less than 5MB is supported.</span>
        }
      </div>
    )
  }

  render() {
    const creativity = this.props.creativity;
    return (
      <div>
        {this.renderCropImage(creativity)}
      </div>
    )
  }
}

const styles = {
  dropzone: {
    height: '100%',
    width: '100%',
    backgroundColor: '#fff',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    fontSize: '16px',
  },
  icon: {
    opacity: 0.3,
    height: '64px',
    width: '64px',
  },
  fileInput: {
    display: 'none'
  }
}

export default Dropzone;
