//abhishek360

import React from 'react';
import { useToasts } from 'react-toast-notifications'

export const withToastHOC = (Component: any) => {
  return (props: any) => {
    const { addToast } = useToasts();

    return <Component addToast={addToast} {...props} />;
  };
};
