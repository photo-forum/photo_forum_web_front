//abhishek360

import React, { Component } from 'react';
import {
  CssBaseline,
  Avatar,
  Button,
  TextField,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  FormControlLabel,
  Checkbox,
  Grid,
  Typography,
  Container,
  CardMedia,
} from '@material-ui/core';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import * as Colors from '../configs/colors';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { withToastHOC } from '../HOC/ToastHOC';

import BackgroundImage from '../assets/background_register.png';
// function Copyright() {
//   return (
//     <Typography variant="body2" color="textSecondary" align="center">
//       {'Copyright © '}
//       <Link color="inherit" href="https://material-ui.com/">
//         Your Website
//       </Link>{' '}
//       {new Date().getFullYear()}
//       {'.'}
//     </Typography>
//   );
// }

class SignUp extends Component {
  state = {
    interest: '',
    first_name: '',
    last_name: '',
    phone: '',
    email: '',
    dob: '',
    gender: '',
    password: '',
    confPassword: '',
  }

  handleInterest = (event, interest) => {
    this.setState({
      interest: interest,
    });
  }

  verifyRegister = () => {
    const { first_name, last_name, phone, dob, gender, password, confPassword, email, interest } = this.state;

    if(interest===''){
      this.props.addToast(
        'Select your Interest!',
        {appearance: 'warning', placement: 'top-center'}
      );
      return;
    }
    if(first_name===''){
      this.props.addToast(
        'First Name is Required!',
        {appearance: 'warning', placement: 'top-center'}
      );
      return;
    }
    else if(email===''){
      this.props.addToast(
        'Email is Required!',
        {appearance: 'warning', placement: 'top-center'}
      );
      return;
    }
    else if(email.search('@')<0){
      this.props.addToast(
        'Enter a valid Email!',
        {appearance: 'warning', placement: 'top-center'}
      );
      alert('Enter a valid Email!');
      return;
    }
    else if(phone===''){
      this.props.addToast(
        'Phone is Required!',
        {appearance: 'warning', placement: 'top-center'}
      );
      alert('Phone is Required!');
      return;
    }
    else if(dob ===''){
      this.props.addToast(
        'Enter a valid Date of Birth!',
        {appearance: 'warning', placement: 'top-center'}
      );
      return;
    }
    else if(gender<1){
      this.props.addToast(
        'Gender is Required!',
        {appearance: 'warning', placement: 'top-center'}
      );
      return;
    }
    else if(password===''){
      this.props.addToast(
        'Password is Required!',
        {appearance: 'warning', placement: 'top-center'}
      );
      return;
    }
    else if(password.length<8){
      this.props.addToast(
        'Password is Too Short!',
        {appearance: 'warning', placement: 'top-center'}
      );
      return;
    }
    else if(password!==confPassword){
      this.props.addToast(
        'Password Mismatch!',
        {appearance: 'warning', placement: 'top-center'}
      );
      return;
    }
    else{
      this.props.handleRegister({
        firstName: first_name,
        lastName: last_name,
        email: email,
        phone: phone,
        dob: dob,
        gender: gender,
        password: password,
        interest: interest
      });
    }
  }

  render(){
    const curDate = new Date();
    const maxDate = (curDate.getFullYear()-16)+'-'+curDate.getMonth()+"-"+curDate.getDate();
    return (
      <div style={styles.container}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={2}>

        </Grid>
        <Grid item xs={12} sm={5}>
          <Typography
            component="h1"
            variant="h3"
            style = {{ marginTop: 200}}
          >
            Select Your Primary Interest
          </Typography>
          <ToggleButtonGroup
            style = {{ margin: 60}}
            value={this.state.interest}
            exclusive
            onChange={this.handleInterest}
          >
            <ToggleButton
              value = 'photography'
              style = {{ height: 140, width: 140 }}
            >
              <CardMedia
                component = "img"
                alt = 'Photography'
                style = {{ height: 140, width: 140 }}
                image = 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/card_photo.png'
                tittle = "Photography"
              />
            </ToggleButton>
            <ToggleButton
              value = 'architecture'
              style = {{ height: 140, width: 140 }}
            >
              <CardMedia
                component = "img"
                alt = "Architecture"
                style = {{ height: 140, width: 140 }}
                image = 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/card_arch.png'
                tittle = "Architecture"
              />
            </ToggleButton>
            <ToggleButton
              value = 'Arts'
              style = {{ height: 140, width: 140 }}
            >
              <CardMedia
                component = "img"
                alt = "Arts"
                style = {{ height: 140, width: 140 }}
                image = 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/card_art.png'
                tittle = "Arts"
              />
            </ToggleButton>
          </ToggleButtonGroup>
        </Grid>
        <Grid item xs={12} sm={5}>
          <Container  component="main" maxWidth='xs'>
            <CssBaseline />
            <div style={styles.paper}>
              <Avatar style={styles.avatar}>
                <ExitToAppIcon />
              </Avatar>
              <Typography component="h1" variant="h4">
                Create an Account
              </Typography>
              <Typography component="h1" variant="h6">
                Be a part of OurForum Community.
              </Typography>
              <br/>
              <br/>
              <div style={styles.form} noValidate>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      autoComplete="fname"
                      onChange = {
                        (event) => this.setState({first_name: event.target.value})
                      }
                      name = "firstName"
                      variant = "outlined"
                      required
                      fullWidth
                      id = "firstName"
                      label = "First Name"
                      autoFocus
                      inputProps={
                        {maxLength: 15}
                      }
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      variant = "outlined"
                      fullWidth
                      id = "lastName"
                      label = "Last Name"
                      name = "lastName"
                      autoComplete = "lname"
                      onChange = {
                        (event) => this.setState({last_name: event.target.value})
                      }
                      inputProps={
                        {maxLength: 15}
                      }
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant = "outlined"
                      required
                      fullWidth
                      id = "email"
                      label = "Email Address"
                      name = "email"
                      autoComplete = "email"
                      onChange = {
                        (event) => this.setState({email: event.target.value})
                      }
                      inputProps={
                        {maxLength: 40}
                      }
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant = "outlined"
                      required
                      fullWidth
                      id = "phone"
                      label = "Phone No."
                      name = "phone"
                      autoComplete = "phone"
                      onChange = {
                        (event) => this.setState({phone: event.target.value})
                      }
                      inputProps={
                        {maxLength: 10}
                      }
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      onChange = {(event) => this.setState({dob: event.target.value})}
                      name= "dob"
                      type= "date"
                      InputLabelProps= {{
                        shrink: true,
                      }}
                      inputProps={{
                        max: maxDate,
                      }}
                      variant= "outlined"
                      required
                      fullWidth
                      id= "dob"
                      label= "Date of Birth"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <FormControl
                      variant="outlined"
                      required
                      fullWidth
                    >
                      <InputLabel id= 'gender_select_label'>Gender</InputLabel>
                      <Select
                        labelid = 'gender_select_label'
                        id="gender"
                        variant= "outlined"
                        value = {this.state.gender}
                        labelWidth={70}
                        onChange= {
                          (event) => this.setState({gender: event.target.value})
                        }
                      >
                        <MenuItem value= 'm'>Male</MenuItem>
                        <MenuItem value= 'f'>Female</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant = "outlined"
                      required
                      fullWidth
                      name = "password"
                      label = "Password"
                      type = "password"
                      autoComplete = "current-password"
                      onChange = {
                        (event) => this.setState({password: event.target.value})
                      }
                      inputProps={
                        {maxLength: 15}
                      }
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      variant = "outlined"
                      required
                      fullWidth
                      name = "confPassword"
                      label = "Confirm Password"
                      type = "password"
                      autoComplete = "current-password"
                      onChange = {
                        (event) => this.setState({confPassword: event.target.value})
                      }
                      inputProps={
                        {maxLength: 15}
                      }
                    />
                  </Grid>
                  <br/>
                  <Grid item xs={12}>
                    <FormControlLabel
                      control = {<Checkbox value = "agreeTerms" color = "primary" />}
                      label = "I agree to the terms and conditions of letterBox."
                    />
                  </Grid>
                </Grid>
                <br/>
                <Button
                  fullWidth
                  variant = "contained"
                  color = "primary"
                  onClick = {() => this.verifyRegister()}
                  style = {styles.submit}
                >
                  Sign Up
                </Button>
              </div>
            </div>
          </Container>
        </Grid>
      </Grid>
      </div>
    );
  }
}

const styles = {
  container: {
    width: '100%',
    minHeight: '87vh',
    paddingTop: 50,
    paddingBottom: 50,
    alignItems: 'center',
    backgroundImage: `url(${BackgroundImage})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    zIndex: -1,
    backgroundRepeat: 'no-repeat',
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    backgroundColor: Colors.FOREGROUND,
    marginBottom: 20,
  },
  form: {
    width: '100%',
    marginTop: 10,
  },
  submit: {
    color: 'white',
    background: Colors.FOREGROUND,
  },
}

export default withToastHOC(SignUp);
