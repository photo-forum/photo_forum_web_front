import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import store from './configs/store';
import Main from './containers/Main';
import { ToastProvider } from 'react-toast-notifications'
//import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <Provider store={ store }>
    <ToastProvider
      autoDismissTimeout = {2000}
      autoDismiss = {true}
    >
      <Main />
    </ToastProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.unregister();
