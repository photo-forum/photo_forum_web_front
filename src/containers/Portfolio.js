//abhishek360
import React, { Component } from 'react';
import {

} from '@material-ui/core';
import { connect } from 'react-redux';

import IntroSection from '../components/Portfolio/IntroSection';
import CreativitySection from '../components/Portfolio/CreativitySection';
import BlogSection from '../components/Portfolio/BlogSection';
import DisplayImage from '../components/Portfolio/DisplayImage';

import {

} from '../actions/UserStateActions';

import {
  fetchPhotoList
} from '../actions/PhotoStateActions';

import {requireAuth} from "../HOC/requireAuth";

class Portfolio extends Component{
  componentDidMount(){
    this.props.fetchPhotoList(1);
  }

  state = {
    showCreativityDetails: false,
  }

  toggleCreativityDetails = (data) => {
    const showCreativityDetails = this.state.showCreativityDetails;
    this.setState({
      showCreativityDetails: !showCreativityDetails,
      selectedCreativityDetails: data,
    })
  }

  handleError = (ev) => {
    ev.target.src = 'https://img.icons8.com/wired/128/000000/circled-user.png';
  }

  render(){
    //const portfolioId = this.props.portfolioId;
    return (
      <div style = {styles.container}>
        {this.state.showCreativityDetails &&
          <DisplayImage
            creativityDetails = {this.state.selectedCreativityDetails}
            toggleUpdateCreativity = {this.toggleUpdateCreativity}
            toggleCreativityDetails = {this.toggleCreativityDetails}
          />
        }
        <IntroSection/>
        <div style = {styles.bottomContainer}>
          <CreativitySection
            toggleCreativityDetails = {this.toggleCreativityDetails}
          />
          <BlogSection/>
        </div>
      </div>
    );
  }
}

const styles = {
  container: {
    paddingTop: 20,
    width: '100vw'
  },
  photosCard: {
    minWidth: '48%'
  },
  bottomContainer: {
    marginTop: 30,
    width: '70%',
    marginLeft: '15%',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'horizontal',
  },
};

const mapStateToProps = ({ userDetails }) => ({
   userDetails
 });

export default connect(mapStateToProps, {
  fetchPhotoList
})(requireAuth(Portfolio));
