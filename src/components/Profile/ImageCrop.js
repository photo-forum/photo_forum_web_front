import React from 'react';
import {
  Button,
} from '@material-ui/core';
import {
  Close,
  Check
} from '@material-ui/icons';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import '../style.css';
import { v4 as uuidv4 } from 'uuid';
import * as Colors from '../../configs/colors';

class ImageCrop extends React.Component {
  state = {
    crop: {
      aspect: 1,
    },
  }

  onImageLoaded = image => {
    const {showPopup, showAddCreativity} = this.props
    if(showPopup){
      if(image.width> image.height)
        this.setState({ crop: { aspect: 1, height: image.height }});
      else
        this.setState({ crop: { aspect: 1, width: image.width }});
    }

    if(showAddCreativity){
      this.setState({crop: {x: 0, y: 0, height: image.height, width: image.width}})
    }

    return false; // Return false when setting crop state in here.
  };

  compress = (file, dimns) => {
    const {x, y, height, width} = this.state.crop;
    const timestamp = new Date().getTime().toString();
    const fileName = uuidv4()+'_'+timestamp+'.'+(file.name).split('.')[1];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = event => {
        const img = new Image();
        img.src = event.target.result;
        img.onload = () => {
          // console.log('image dimns,', dimns.width, dimns.height);
          // console.log('image natural,', img.naturalWidth, img.naturalHeight);
          const elem = document.createElement('canvas');
          const scale = img.naturalHeight/dimns.height;
          elem.width = width*scale;
          elem.height = height*scale;
          const ctx = elem.getContext('2d');
          // img.width and img.height will contain the original dimensions
          ctx.drawImage(img, x*scale, y*scale, width*scale, height*scale, 0, 0, width*scale, height*scale);
          const newCanvas = document.createElement('canvas');
          newCanvas.width = dimns.width;
          newCanvas.height = dimns.height;
          const newCtx = newCanvas.getContext('2d');

          newCtx.drawImage(elem, 0, 0, dimns.width, dimns.height);
          newCtx.canvas.toBlob((blob) => {
            const file = new File([blob], fileName, {
                type: 'image/jpeg',
                lastModified: Date.now()
            });
            this.props.setCropImage(file);
          }, 'image/jpeg', 1);

        }
        reader.onerror = error => console.log(error);
    };
  }

  calculateImgDimns = (file) => {
    const height = file.height;
    const width = file.width;

    //console.log('dimensions in file', file);

    if((height>=width)&&(height>720)){
      const newHeight = 720;
      const newWidth = (newHeight/height)*width;
      return { height: newHeight, width: newWidth };
    }
    else if((width>height)&&(width>1080)){
      const newWidth = 1080;
      const newHeight = (newWidth/width)*height;
      return { height: newHeight, width: newWidth };
    }

    return { height, width };
  }


  render() {
    const {file, imgSrc, imgDimns, showPopup} = this.props;
    const dimns = this.calculateImgDimns(imgDimns);
    return (
      <div className = 'popup'>
        <div className = 'popup_inner_img_crop'>
          {this.props.imgSrc &&
            <ReactCrop
              style = {{...styles.cropContainer, ...dimns}}
              src = {imgSrc}
              crop = {this.state.crop}
              onChange = {newCrop => this.setState({crop: newCrop})}
              onImageLoaded = {this.onImageLoaded}
              circularCrop = {showPopup? true: false}
            />
          }
          <div style = {styles.footer}>
            <Button
              style = {styles.button}
              onClick={this.props.closePopup}
            >
              <Close/> Cancel
            </Button>
            <Button
              style = {styles.button}
              onClick={() => this.compress(file, dimns)}
            >
              <Check/> Crop
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  cropContainer: {
    marginTop: 30,
    padding: '1px',
    border: '2px solid rgb(0, 0, 0)',
  },
  textField: {
    margin: 10,
  },
  footer: {
    position: 'fixed',
    bottom: '5%',
    minWidth: '60%',
    borderRadius: 5,
    background: Colors.AQUAMARINE,
  },
  button: {
    margin: 10,
    minWidth: 100,
    background: 'purple',
  },
};

export default ImageCrop;
