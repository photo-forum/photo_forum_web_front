//abhishek360
import React, { Component } from 'react';
import Dropzone from '../Dropzone';
import Progress from '../Progress';
import {
  IconButton,
  TextField,
  Button,
} from '@material-ui/core';
import {
  CheckCircle,
  Close,
  Check
} from '@material-ui/icons';
import * as Colors from '../../configs/colors';
import '../style.css';
import { withToastHOC } from '../../HOC/ToastHOC';

class AddCreativity extends Component {
  state = {
    caption: '',
    desc: '',
  }

  renderProgress = () => {
    const uploadProgress = this.props.uploadProgress;
    const uploading = this.props.uploading;
    const successfullUploaded = this.props.successfullUploaded
    //console.log('upload status',uploadProgress );
    if(uploading || successfullUploaded){
      return (
        <div style = {styles.progressWrapper}>
          <Progress progress = {uploadProgress ? uploadProgress : 0}/>
          <CheckCircle
            style = {{
              opacity:
                successfullUploaded ? 0.8 : 0
            }}
          />
        </div>
      );
    }
  }

  verifyNewCreativity  = (file) => {
    const {caption, desc} = this.state;
    if(caption===''){
      this.props.addToast('Write a caption!', {appearance: 'warning'});
    }
    else if(caption.length>50){
      this.props.addToast(
        'Caption must be limited to 50 characters!',
        {appearance: 'warning'}
      );
    }
    else if(desc===''){
      this.props.addToast(
        'Description too short!',
        {appearance: 'warning'}
      );
    }
    else if(desc.length>200){
      this.props.addToast(
        'Description must be limited to 200 characters!',
        {appearance: 'warning'}
      );
    }
    else if(!file){
      this.props.addToast(
        'You must select an image!',
        {appearance: 'warning'}
      );
    }
    else{
      this.props.addCreativity(
        {
          caption,
          details: desc,
          photoUrl: file.name,
        },
        file
      );
    }
  }

  renderDropzone = (imgSrc) => {
    if(imgSrc&&imgSrc.length>0){
      return (
        <img
          src = {imgSrc}
          alt=""
          style = {styles.image}
        />
      )
    } else {
      return (
        <Dropzone
          onFilesAdded = {this.props.onFilesAdded}
          imgSrc = {imgSrc}
          disabled = {
            this.props.uploading || this.props.successfullUploaded
          }
          creativity = {true}
        />
      )
    }
  }

  render() {
    const { file, fileName, imgSrc } = this.props;
    return (
      <div className = 'popup'>
        <div className = 'popup_inner_creativity_add'>
          <div className = 'popup_close_creativity_add'>
            <IconButton
              onClick={this.props.closePopup}
            >
              <Close/>
            </IconButton>
          </div>
          <h2 align = "center" style = {{padding: 5, margin: 5}}> Upload New Creativity </h2>
          <h4 align = "center" style = {{padding: 5, margin: 5}}> Let the world know, how ceative you can be. </h4>
          <div align = 'center' style = {styles.dropzone}>
            {this.renderDropzone(imgSrc)}
          </div>
          <br/>
          {file &&
            <div style = {styles.row}>
              <div style = {styles.fileName}>File Name: {fileName}</div>
              {this.renderProgress()}
            </div>
          }
          <TextField
            style = { styles.textField }
            label = "Image Caption Here :"
            variant="outlined"
            onChange = {(event) =>
              this.setState({caption: event.target.value})
            }
            inputProps={
              {maxLength: 50}
            }
          />
          <TextField
            style = { styles.textField }
            label = "Write Short Description Here :"
            multiline
            rows="4"
            variant="outlined"
            onChange = {
              (event) => this.setState({desc: event.target.value})
            }
            inputProps={
              {maxLength: 200}
            }
          />
          <div
            align = 'center'
            style = {styles.footer}
          >
            <Button
              style={styles.footerButton}
              onClick = {() =>
                this.props.clearImgSelect()
              }
            >
              <Close/> Clear
            </Button>
            <Button
              style={styles.footerButton}
              onClick={(event) => {
                  this.verifyNewCreativity(file);
                }
              }
            >
              <Check/> Save & Upload
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  content: {
    display: 'flex',
    flexDirection: 'row',
    paddingTop: '16px',
    width: '100%',
  },
  dropzone: {
    height: '50%',
    width: '95%',
    marginLeft: 20,
    padding: 5,
    backgroundColor: '#fff',
    border: '2px dashed rgb(187, 186, 186)',
    borderRadius: 5,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    maxHeight: '100%',
    borderRadius: 5,
    backgroundColor: '#fff',
    alignItems: 'center',
    objectFit: 'cover'
  },
  textField: {
    width: '95%',
    margin: 10,
  },
  footer: {
    position: 'fixed',
    bottom: '5%',
    width: '60%',
    borderRadius: 5,
    background: Colors.AQUAMARINE,
  },
  footerButton: {
    marginTop: 10,
    marginBottom: 10,
    marginRight: '15px',
    color: Colors.WHITE,
    background: Colors.PURPLE,
  },
  row: {
    marginLeft: '20px',
    marginTop: '10px',
  },
  progressWrapper: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  fileName: {
    marginBottom: '8px',
    fontSize: '16px',
    color: 'blue',
  }
}

export default withToastHOC(AddCreativity);
