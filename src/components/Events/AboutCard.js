//abhishek360
import React, { Component } from 'react';
import {
  Card,
  Typography,
  CardContent,
  CardMedia
} from '@material-ui/core';
import * as Colors from '../../configs/colors';

class AboutCard extends Component{
  render(){
    const {img, desc, obj} = this.props.categoryDetails;
    return (
      <div
        style = {styles.container}
      >
        <Card style = {styles.photosCard}>
          <CardContent
            align = 'center'
            style = {{ padding: 5, backgroundColor: Colors.FOREGROUND_2,}}
          >
            <div style = {{display: 'inline', fontSize: 50}}>I</div>
            <div style = {{display: 'inline', fontSize: 30}}>NTRODUCTION</div>
          </CardContent>
          <CardContent
            style = {{padding: 5, display: 'flex', flexDirection: 'horizontal'}}
          >
            <div style= {{flex: 1, marginTop: 20 }}>
              <div align = 'center' style = {{margin: 5, padding: 5,}}>
                <CardMedia
                  align = 'center'
                  component = "img"
                  alt = 'camera'
                  style = {styles.categoryImg}
                  image = { img }
                />
              </div>
              <br/>
              <Typography style= {styles.introText}>
                {desc}
              </Typography>
            </div>
            <div style= {styles.objText}>
              {obj.map((item, key) => {
                return (
                  <div key= {key} style = {styles.keyPoints}>
                    {item}
                  </div>
                )
              })}
            </div>
          </CardContent>
        </Card>
      </div>
    );
  }
}

const styles = {
  container: {
    width: '100vw'
  },
  photosCard: {
    marginTop: 30,
    marginLeft: '15%',
    width: '70%',
    marginBottom: 30,
  },
  categoryImg: {
    boxShadow: "5px 3px 5px #9E9E9E",
    border: '1px solid black',
    padding: 5,
    height: 180,
    width: 180
  },
  introText: {
    textAlign: 'justify',
    textJustify: 'inter-word',
    padding: 5,
    flex: 1,
    fontSize: 18,
    margin: 20
  },
  objText: {
    flex: 1,
    fontSize: 14,
    margin: 15,
    padding: 10,
  },
  keyPoints: {
    boxShadow: "5px 3px 5px #9E9E9E",
    padding: 10,
    margin: 15,
    backgroundColor: Colors.FOREGROUND_2,
  }
};

export default AboutCard;
