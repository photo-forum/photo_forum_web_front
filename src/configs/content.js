export const category = [
  {
    key: 1,
    value: 'photography',
    title: 'Photography',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/card_photo.png',
    desc: "Foto Forum was established by enthusiastic photographers for photographers. It provides a platform for the finest talents all over the world such thatthey can showcase their unique photographs worldwide. It was created on 14th June, 2019 and it's currently with a large number of active group members (1.6k and counting) from all over the world.",
    obj: [
      "We stress on motivating photographers to come up with new thought provoking ideas and fresh perspectives.",
      "We provide an open and friendly environment for all and giving them the opportunities to learn from their fellow colleagues.",
      "We try to inculcate a feeling of uniqueness in them.",
      "We want the photo-enthusiastics to get their own photography skills appreciated and recognized.",
      "Our aim is to promote photography as a form of visual art.",
      "We intend to arrange Competitions, Photowalks, Workshops and Exhibitions in different cities in order to provocate both amateur and professional photographers."
    ]
  },
  {
    key: 2,
    value: 'architecture',
    title: 'Architecture',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/card_arch.png',
    desc: 'World of Architecture!'
  },
  {
    key: 3,
    value: 'art',
    title: 'Art',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/card_art.png',
    desc: 'World of Art!'
  },
]

export const socialLinks = {
  fb: 'https://www.facebook.com/ourforum123/',
  insta: 'https://www.instagram.com/our.forum/',
}

export const homeSlide = [
  {
    key: 1,
    value: 'img1',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/slide1.jpg',
  },
  {
    key: 2,
    value: 'img2',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/slide2.jpg',
  },
  {
    key: 3,
    value: 'img3',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/home_assests/slide3.jpg',
  },
]

export const photoEvents = [
  {
    title: 'photo event 1',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' photo event 1 desc'
  },
  {
    title: 'photo event 2',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'photo event 2 desc'
  },
  {
    title: 'photo event 3',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'photo event 3 desc'
  },
  {
    title: 'photo event 4',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' photo event 1 desc'
  },
]

export const photoSlides = [
  {
    key: 1,
    value: 'img1',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/photography_assets/photo_slide_1.jpg',
  },
  {
    key: 2,
    value: 'img2',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/photography_assets/photo_slide_2.jpg',
  },
  {
    key: 3,
    value: 'img3',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/photography_assets/photo_slide_3.jpg',
  },
]

export const archiSlides = [
  {
    key: 1,
    value: 'img1',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/archi_assets/archi_slide_1.jpg',
  },
  {
    key: 2,
    value: 'img2',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/archi_assets/archi_slide_2.jpg',
  },
  {
    key: 3,
    value: 'img3',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/archi_assets/archi_slide_3.jpg',
  },
]

export const artSlides = [
  {
    key: 1,
    value: 'img1',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/art_assets/art_slide_1.jpg',
  },
  {
    key: 2,
    value: 'img2',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/art_assets/art_slide_2.jpg',
  },
  {
    key: 3,
    value: 'img3',
    img: 'https://ourforum-storage.s3.us-east-2.amazonaws.com/web_assests/art_assets/art_slide_3.jpg',
  },
]

export const archiEvents = [
  {
    title: 'archi event 1',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' archi event 1 desc'
  },
  {
    title: 'archi event 2',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'archi event 2 desc'
  },
  {
    title: 'archi event 3',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'archi event 3 desc'
  },
  {
    title: 'archi event 4',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' archi event 1 desc'
  },
]

export const artsEvents = [
  {
    title: 'arts event 1',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' arts event 1 desc'
  },
  {
    title: 'arts event 2',
    img: 'https://images.pexels.com/photos/227675/pexels-photo-227675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'arts event 2 desc'
  },
  {
    title: 'arts event 3',
    img: 'https://images.pexels.com/photos/990824/pexels-photo-990824.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    desc: 'arts event 3 desc'
  },
  {
    title: 'arts event 4',
    img: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
    desc: ' arts event 1 desc'
  },
]
