//abhishek360
import React, { Component } from 'react';
import {
  IconButton,
  Typography,
} from '@material-ui/core';
import { connect } from 'react-redux';
import {
  Close,
} from '@material-ui/icons';
import { RatingSlider } from './RatingSlider';
import * as Colors from '../../configs/colors';
import './style.css';
import {
  putRemovePhoto,
  putUpdatePhoto,
} from '../../actions/PhotoStateActions';
import {
  renderDateTime
} from '../../helpers/UsefullFunctions';

class DisplayImage extends Component {
  state = {
    value: 0,
    visible: true,
  }

  componentDidMount() {
    this.fadeOutTime();
  }

  handleChange = (event, value) => {
    this.setState({
      value: value
    })
  }

  handleImgClick = () => {
    const visible = this.state.visible;
    this.setState({
      visible: !visible
    })
  }

  fadeOutTime  = () => {
    setTimeout(() => {
      this.setState({
        visible: false
      });
    }, 1000);
  }

  render() {
    const visible = this.state.visible;
    const { caption, details, photoUrl, createdAt, id } = this.props.creativityDetails;
    return (
      <div className = 'popup'>
        <div className = 'popup_close_image'>
          <IconButton
            style = {{backgroundColor: Colors.WHITE}}
            onClick={() => this.props.toggleCreativityDetails({})}
          >
            <Close/>
          </IconButton>
        </div>
        <div className = 'popup_inner_image'>
          <div align = 'center' style = {styles.dropzone}>
            <div
              className={visible ? 'fadeIn': 'fadeOut'}
              style = {styles.caption}
            >
              <Typography
                align = 'left'
                style = {{margin: '10px 20px 0px 20px', fontSize: 25}}
              >
                { caption }
              </Typography>
              <Typography
                align = 'left'
                style = {{
                  padding: 5,
                  margin: '0px 20px 10px 20px',
                  fontSize: 12
                }}
              >
                {renderDateTime(createdAt)}
              </Typography>
            </div>
            <img
              onClick = {this.handleImgClick}
              src = {photoUrl}
              alt=""
              style = {styles.image}
            />
            <div
              className={visible ? 'fadeIn': 'fadeOut'}
              align = 'left'
              style = {styles.details}
            >
              <Typography
                style = {{ margin: '10px 20px 10px 20px', fontSize: 16}}
              >
                {details}
              </Typography>
            </div>
          </div>
        </div>
        <div
          align = 'center'
          style = {styles.footer}
        >
          <div
            style = {{
              width: '60%',
              padding: 5,
              display: 'flex',
              color: Colors.WHITE
            }}
          >
            <RatingSlider
              min = {0}
              max = {10}
              step = {1}
              style = {{marginTop: 10, flex: 2}}
              marks
              defaultValue = {8}
              onChange = {this.handleChange}
              valueLabelDisplay="on"
            />
            <div style = {{flex: 1}}>
              <div style = {styles.ratingCircle}>
                178
              </div>
            </div>
            <div style = {{flex: 1}}>
              <div style = {styles.ratingCircle}>
                6.7
              </div>
            </div>
          </div>
          <div
            style = {{
              width: '60%',
              padding: 5,
              display: 'flex',
              color: Colors.WHITE
            }}
          >
            <div style = {{fontSize: 20, flex:2}}>
              Rate My Creativity
            </div>
            <div style = {{fontSize: 20, flex: 1}}>
              Total Ratings
            </div>
            <div style = {{fontSize: 20, flex: 1}}>
              Avg Rating
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  content: {
    display: 'flex',
    flexDirection: 'row',
    paddingTop: '16px',
    width: '100%',
  },
  caption: {
    width: '100%',
    color: Colors.WHITE,
    top: 0,
    backgroundColor: "rgba(0,0,0, 0.7)",
    position: 'absolute'
  },
  details: {
    color: Colors.WHITE,
    width: '100%',
    bottom: 0,
    backgroundColor: "rgba(0,0,0, 0.7)",
    position: 'absolute'
  },
  dropzone: {
    width: 'auto',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: '80vh',
    borderRadius: 5,
    alignItems: 'center',
  },
  ratingCircle: {
    width: 50,
    height: 50,
    display: 'flex',
    fontSize: 20,
    justifyContent: 'center',
    flexDirection: 'column',
    borderRadius: 25,
    border: `1px solid ${Colors.WHITE}`
  },
  footer: {
    position: 'fixed',
    bottom: 0,
    width: '100%',
    padding: '20px',
    borderRadius: 5,
    backgroundColor: '#21D4FD',
    backgroundImage: 'linear-gradient(19deg, #21D4FD 0%, #B721FF 100%)',
  }
}

const mapStateToProps = ({ photoList }) => ({
  photoList
 });

export default connect(mapStateToProps, {
  putRemovePhoto,
  putUpdatePhoto
})(DisplayImage);
