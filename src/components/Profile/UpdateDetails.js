//abhishek360
import React, { Component } from 'react';
import {
  Button,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem
} from '@material-ui/core';
import Upload from './Upload';
import { connect } from 'react-redux';
import * as Colors from '../../configs/colors';
import { withToastHOC } from '../../HOC/ToastHOC';
import {
  putDetailsUpdate,
  postChangePsswd,
  putUpdateUsername
} from '../../actions/UserStateActions';

class UpdateDetails extends Component{
  state = {
    firstName: '',
    lastName: '',
    gender: '',
    bio: '',
    interest: '',
    oldPassword: '',
    newPassword: '',
    confNewPassword: '',
    username: ''
  }

  verifyPassChange = () => {
    const { oldPassword, newPassword, confNewPassword } = this.state;

    if(oldPassword===''){
      this.props.addToast(
        'Old Password is Required!',
        {appearance: 'warning'}
      );
    }
    else if(oldPassword.length<6){
      this.props.addToast(
        'OPassword too short!',
        {appearance: 'warning'}
      );
    }
    else if(newPassword.length<6){
      this.props.addToast(
        'Password min length is 6!',
        {appearance: 'warning'}
      );
    }
    else if(newPassword !== confNewPassword){
      this.props.addToast(
        'New Password did not match!',
        {appearance: 'warning'}
      );
    }
    else{
      this.props.postChangePsswd(oldPassword, newPassword);
      this.props.closePopup();
    }
  }


  sendUpdate = (updateCode) => {
    const { firstName, lastName, gender, interest, bio, username } = this.state;

    switch(updateCode){
      case 0 :
        if(lastName!==''&&firstName!=='')
          this.props.putDetailsUpdate({firstName, lastName});
        else if(firstName!=='')
          this.props.putDetailsUpdate({firstName});
        else if(lastName!=='')
          this.props.putDetailsUpdate({lastName});

        this.props.closePopup();
        break;

      case 2 :
        if(gender!=='')
          this.props.putDetailsUpdate({gender});

        this.props.closePopup();
        break;

      case 3 :
        if(interest!=='')
          this.props.putDetailsUpdate({interest});

        this.props.closePopup();
        break;

      case 1 :
        if(bio!=='')
          this.props.putDetailsUpdate({bio});

        this.props.closePopup();
        break;

      case 4 :
          this.verifyPassChange();
        break;

      case 5 :
          const file = this.props.file;
          if(file){
            this.props.uploadProPic(file);
          }
          else {
            this.props.addToast(
              'You must select an Image!',
              {appearance: 'warning'}
            );
          }
        break;

        case 6 :
          if(username&&username!=='')
            this.props.putUpdateUsername({username});

          this.props.closePopup();
          break;

      default :  return null;
    }
  }

  handleClick = (event) => {
    alert('button clicked');
  }

  renderTextField = (updateCode) => {
    switch(updateCode){
      case 0 :
        return (
          <div>
            <TextField
              id = "firstNameTextFieldUpdate"
              style = { styles.textField }
              label = "First Name"
              onChange = {(event) =>
                this.setState({firstName: event.target.value})
              }
            />
            <TextField
              style = { styles.textField }
              id = "lastNameTextFieldUpdate"
              label = "Last Name"
              onChange = {(event) =>
                this.setState({lastName: event.target.value})
              }
            />
          </div>
        )
      case 1 :
        return (
          <TextField
            style = { styles.textField }
            id = "outlined-multiline-static"
            label = "Write Short Bio :"
            multiline
            rows="4"
            variant="outlined"
            onChange = {(event) => this.setState({bio: event.target.value})}
          />
        )
      case 2 :
        return (
          <FormControl
            variant="outlined"
            required
            fullWidth
            style = { styles.textField }
          >
            <InputLabel id= 'gender_select_label'>Gender</InputLabel>
            <Select
              labelid = 'gender_select_label'
              id="gender"
              variant= "outlined"
              value = {this.state.gender}
              labelWidth={70}
              onChange= {(event) => this.setState({gender: event.target.value})}
            >
              <MenuItem value= 'm'>Male</MenuItem>
              <MenuItem value= 'f'>Female</MenuItem>
            </Select>
          </FormControl>
        )
      case 3 :
        return (
          <FormControl
            variant="outlined"
            required
            style = { styles.textField }
            fullWidth
          >
            <InputLabel id= 'interest_select_label'>Your Interest:</InputLabel>
            <Select
              labelid = 'interest_select_label'
              id="gender"
              variant= "outlined"
              value = {this.state.gender}
              labelWidth={90}
              onChange= {(event) => this.setState({interest: event.target.value})}
            >
              <MenuItem value= 'photography'>Photography</MenuItem>
              <MenuItem value= 'architecture'>Architecture</MenuItem>
              <MenuItem value= 'arts'>Arts</MenuItem>
            </Select>
          </FormControl>
        )
      case 4 :
        return (
          <div>
            <TextField
              id="oldPassTextFieldUpdate"
              style = { styles.textField }
              type = "password"
              label = "Old Password"
              onChange = {(event) => this.setState({oldPassword: event.target.value})}
            />
            <TextField
              style = { styles.textField }
              id="newPassTextFieldUpdate"
              type = "password"
              label = "New Password"
              onChange = {(event) => this.setState({newPassword: event.target.value})}
            />
            <TextField
              style = { styles.textField }
              id="confNewPassTextFieldUpdate"
              type = "password"
              label = "Confirm New Password"
              onChange = {(event) => this.setState({confNewPassword: event.target.value})}
            />
          </div>
        )

      case 5 :
        return (
          <div>
            <Upload
              file = {this.props.file}
              fileName = {this.props.fileName}
              imgSrc = {this.props.imgSrc}
              uploading = {this.props.uploading}
              successfullUploaded = {this.props.successfullUploaded}
              onFilesAdded = {this.props.onFilesAdded}
              uploadProgress = {this.props.uploadProgress}
            />
          </div>
        )

      case 6:
          return (
            <TextField
              id = "firstNameTextFieldUpdate"
              style = { styles.textField }
              label = "New Username"
              onChange = {(event) =>
                this.setState({username: event.target.value})
              }
            />
          )

      default :
        return <div> </div>
    }
  }

  handleTextChange = (event) => {
    const newValue = event.target.value;
    const id = event.target.id;

    switch (id) {
      case 'usernameTextFieldLogin':
        this.setState({
          username: newValue,
        })
        break;
      case 'passwordTextFieldLogin':
        this.setState({
          password: newValue,
        })
        break;
      default:
    }
  }

  render(){
    return (
      <div>
        <div
          style = { styles.head }
        >
          { this.props.updateCode === 5 ?
              <h3 align = "center"> Change Profile Picture </h3> :
              <h3 align = "center"> Update Details </h3>
          }
        </div>
        <div style={styles.container} >
          {this.renderTextField(this.props.updateCode)}
        </div>
        { this.props.updateCode === 5 &&
          <p style = {{margin: 5}}>
            *Only .jpeg or .png file less than 3MB is supported.
          </p>
        }
        <div
          align = 'right'
          style = {styles.footer}
        >
        { this.props.updateCode === 5 &&
          <Button
            style={styles.footerButton}
            onClick = {() =>
              this.props.clearImgSelect()
            }
          >
            Clear
          </Button>
        }
          <Button
            style={styles.footerButton}
            onClick={(event) => {
                this.sendUpdate(this.props.updateCode);
              }
            }
          >
            Confirm
          </Button>
        </div>
      </div>
    );
  }
}

const styles = {
  container: {
    position: 'absolute',
    width: '100%',
    margin: 10,
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)'
  },
  head: {
    top: '10%',
  },
  textField: {
    width: '70%',
    marginLeft: '15%',
  },
  button: {
    margin: 10,
    width: '75%',
    color: Colors.WHITE,
    background: Colors.PURPLE,
  },
  footer: {
    position: 'absolute',
    bottom: '0%',
    width: '100%',
    borderRadius: 5,
    background: Colors.AQUAMARINE,
  },
  footerButton: {
    marginTop: 10,
    marginBottom: 10,
    marginRight: '15%',
    color: Colors.WHITE,
    background: Colors.PURPLE,
  },
};

const mapStateToProps = ({ userDetails }) => ({ userDetails });

export default connect(mapStateToProps, {
  putDetailsUpdate,
  postChangePsswd,
  putUpdateUsername,
})(withToastHOC(UpdateDetails));
