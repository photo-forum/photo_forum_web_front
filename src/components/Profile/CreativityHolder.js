//abhishek360

import React, { Component } from 'react';
import * as Colors from '../../configs/colors';
import {
  Grid,
  Typography,
  Card,
  CardContent,
  CardActionArea,
  CardMedia,
} from '@material-ui/core';
import { connect } from 'react-redux';
import {
  Add,
} from '@material-ui/icons';
import Pagination from '../Pagination';
import {
  renderDateTime
} from '../../helpers/UsefullFunctions';

class CreativityHolder extends Component{
  renderPhotosList = (item) => {
    return (
      <Grid
        item lg = {3}
        key = { item.id }
      >
      <Card style = {{ margin: 5, boxShadow: "5px 3px 5px 3px #9E9E9E",}}>
        <CardActionArea
          align = 'center'
          style = { styles.actionAreaCard }
          onClick = {()=> this.props.toggleCreativityDetails(item)}
        >
          <div
            style = {styles.caption}
          >
            <Typography
              style = {{fontSize: 14}}
            >
              { item.caption }
            </Typography>
            <Typography
              style = {{fontSize: 10}}
            >
              {renderDateTime(item.createdAt)}
            </Typography>
          </div>
          <CardMedia
            component = "img"
            alt = "photo"
            style = {{display: 'block', margin: 'auto', maxHeight: 180, maxWidth: 240}}
            image = {item.photoUrl}
          />
        </CardActionArea>
      </Card>
    </Grid>
    )
  }
  render(){
    return (
      <div
        style = {styles.container}
      >
        <Card style = {styles.photosCard}>
            <CardContent
              align = 'center'
              style = {{display: 'flex', flexDirection: 'horizontal', backgroundColor: Colors.FOREGROUND_2,}}
            >
              <div
                style = {{ padding: 5, flex: 3, fontSize: 22}}
              >
                Your Creativity List
              </div>
              <div
                style = {{ padding: 5, flex: 0.5, fontSize: 22}}
              >
                Pages:
              </div>
              <Pagination
                totalRecords = {this.props.photoList.count}
                onPageChanged = {(pageData) =>
                  this.props.handlePageChange(pageData.currentPage)
                }
              />
            </CardContent>
            <CardContent style = {{ padding: 5, backgroundColor: Colors.WHITE}}>
              <Grid
                container
                direction = 'row'
                justify = 'space-evenly'
                alignItems = 'center'
              >
                <Grid
                  item lg = {3}
                >
                  <Card style = {{backgroundColor: Colors.AQUAMARINE, margin: 5, padding: 5}}>
                    <CardActionArea
                      style = {{ ...styles.actionAreaCard, minHeight: 175, minWidth: 235,}}
                      onClick = {this.props.toggleAddCreativity}
                    >
                      <div align = 'center'>
                        <Add style = {{height: 70, width: 70}}/>
                        <br/>
                        <Typography
                          style = {{fontSize: 30}}
                        >
                          My New Creativity
                        </Typography>
                      </div>
                    </CardActionArea>
                  </Card>
                </Grid>
                {
                  this.props.photoList.list.map(item =>
                    this.renderPhotosList(item)
                  )
                }
                </Grid>
            </CardContent>
        </Card>
      </div>
    );
  }
}

const styles = {
  container: {
    paddingTop: 10,
    paddingBottom: 10,
  },
  caption: {
    padding: 5,
    width: '100%',
    color: 'white',
    bottom: 0,
    backgroundColor: "rgba(0,0,0, 0.6)",
    position: 'absolute'
  },
  actionAreaCard: {
    minHeight: 180,
    minWidth: 240,
    alignItems: 'center',
    justifyContent: 'center',
    border: '2px solid black'
  },
  photosCard: {
    marginTop: 30,
    marginLeft: '20%',
    width: '60%'
  },
  cardAction: {
    minHeight: 30,
    backgroundColor: Colors.AQUAMARINE,
    justifyContent: 'center',
  },
  button: {
    marginRight: 20,
    color: 'white',
    background: 'purple',
  },
};

const mapStateToProps = ({ photoList }) => ({
  photoList
 });

export default connect(mapStateToProps, {

})(CreativityHolder);
